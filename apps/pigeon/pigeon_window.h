#ifndef IW_PIGEON_WINDOW
#define IW_PIGEON_WINDOW

#include "pigeon_worker.h"

#include <ITAPropagationPathSim/CombinedModel/PropagationEngine.h>
#include <QMainWindow>
#include <QSettings>
#include <QThread>
#include <QTimer>
#include <string>

namespace Ui
{
	class CPigeonWindow;
}

class CPigeonWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit CPigeonWindow( QWidget* parent = 0 );
	~CPigeonWindow( );

	void closeEvent( QCloseEvent* event );

private slots:

	void on_actionQuit_triggered( );
	void on_actionReset_triggered( );
	void on_pushButton_run_clicked( );
	void on_pushButton_geo_input_clicked( );
	void on_pushButton_mat_database_clicked( );
	void on_pushButton_pp_export_clicked( );
	void on_pushButton_viz_geo_output_clicked( );
	void on_pushButton_viz_add_paths_clicked( );

	void UpdateProgress( QString, float );
	void StopWorkerThread( float );
	void ErrorMessage( QString );

private:
	Ui::CPigeonWindow* ui;

	QSettings m_qSettings;
	QThread* m_pWorkerThread;

	CPigeonWorker* m_pWorker;
	ITAPropagationPathSim::CombinedModel::CPathEngine::CSimulationConfig m_oSimConf;
	ITAPropagationPathSim::CombinedModel::CPathEngine::CAbortionCriteria m_oSimAbort;

	void LoadSettings( );
	void StoreSettings( );
	void Reset( );

	void SetGUIElementsEnabled( bool bEnabled );
};

#endif // IW_PIGEON_WINDOW
