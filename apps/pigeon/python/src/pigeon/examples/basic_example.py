from pathlib import Path

import pigeon

cwd = Path.cwd()
file_directory = Path(__file__).parent


def main():
    config = pigeon.PigeonConfig()

    # Define the sources and receivers. The positions are in meters in OpenGL coordinates.
    config.sources.append(pigeon.PigeonSource(position=(-2, 1, 4)))
    config.receivers.append(pigeon.PigeonReceiver(position=(3, 1, 5)))
    config.receivers.append(pigeon.PigeonReceiver(position=(3, 3.5, -9), name="receiver2"))
    config.geometry_file = (
        file_directory / "basic_example.dae"
    )  # or .blend if you use Blender, or any other supported format

    project = pigeon.PigeonProject(
        working_dir=cwd / "basic_example",
        executable_path=None,  # You can specify the path to the Pigeon executable here, if none, it will search for the Pigeon executable
        config=config,
    )

    project.create_filter("propagation_filter.ita")


if __name__ == "__main__":
    main()
