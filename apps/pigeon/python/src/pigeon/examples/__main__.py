import importlib
import sys
from pathlib import Path


def main():
    current_path = Path(__file__).parent

    example_files = list(current_path.glob("*example*.py"))

    if len(sys.argv) == 1:
        for i, example_file in enumerate(example_files):
            print(f"{i}: {example_file.stem}")

        print(f"{len(example_files)}: Exit")

        example_index = int(input("Enter the index of the example you want to run: "))
    else:
        example_index = int(sys.argv[1])

    if example_index == len(example_files):
        return

    if example_index < 0 or example_index >= len(example_files):
        print("Invalid index")
        return

    example_file = example_files[example_index]
    module_name = f"pigeon.examples.{example_file.stem}"
    module = importlib.import_module(module_name)

    example_main = getattr(module, "main", None)
    if example_main is not None:
        example_main()
    else:
        print(f"Could not find 'main' function in module '{module_name}'")


if __name__ == "__main__":
    main()
