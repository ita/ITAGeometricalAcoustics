from pathlib import Path

import pigeon

cwd = Path.cwd()
file_directory = Path(__file__).parent


def main():
    config = pigeon.PigeonConfig()

    config.geometry_file = (
        file_directory / "wedge.blend"
    )

    project = pigeon.PigeonProject(
        working_dir=cwd / "wedge_example",
        executable_path=None,  # You can specify the path to the Pigeon executable here, if none, it will search for the Pigeon executable
        config=config,
    )

    project.create_filter("propagation_filter.ita")


if __name__ == "__main__":
    main()
