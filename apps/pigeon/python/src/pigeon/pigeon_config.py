from __future__ import annotations

import dataclasses
import math
from pathlib import Path


@dataclasses.dataclass
class PigeonSource:
    """
    Represents a source in the Pigeon simulation.

    Attributes:
        name (str): The name of the source.
        position (tuple[float, float, float]): The position of the source in 3D space in OpenGL coordinates, in meter
        rotation (tuple[float, float, float, float]): The rotation of the source in quaternion format.
        directivity (str): The directivity name of the source.
    """

    name: str = "source"
    position: tuple[float, float, float] = (0, 0, 0)
    rotation: tuple[float, float, float, float] = (0, 0, 0, 1)
    directivity: str = ""


@dataclasses.dataclass
class PigeonReceiver:
    """
    Represents a receiver in the Pigeon system.

    Attributes:
        name (str): The name of the receiver.
        position (tuple[float, float, float]): The position of the receiver in 3D space in OpenGL coordinates, in meter.
        rotation (tuple[float, float, float, float]): The rotation of the receiver as a quaternion.
        directivity (str): The directivity name of the receiver.
    """

    name: str = "receiver"
    position: tuple[float, float, float] = (0, 0, 0)
    rotation: tuple[float, float, float, float] = (0, 0, 0, 1)
    directivity: str = ""


@dataclasses.dataclass
class ExpertConfig:
    """
    Configuration class for expert settings.
    """

    # TODO: the defaults are different between the C++ and Matlab implementation
    # abort criteria
    level_drop_threshold: float = 124
    reflection_penalty: float = -10 * math.log10(0.8)  # for conservative sim set to 0
    diffraction_penalty: float = 2.5  # for conservative sim set to 0
    max_accumulated_diffraction_angle: float = -1  # disabled

    # sim config
    only_neighbored_edge_diffraction: bool = False
    diffraction_only_into_shadowed_edges: bool = False
    filter_not_visible_paths_between_edges: bool = False
    filter_emitter_to_edge_intersected_paths: bool = False
    filter_sensor_to_edge_intersected_paths: bool = False
    intersection_test_resolution: float = 0.001
    max_apex_calculation_iteration: int = 5


@dataclasses.dataclass
class PigeonConfig:
    """
    Configuration class for Pigeon.

    Attributes:
        geometry_file (Path): Path to the geometry file.
        material_database (Path): Path to the material database.
        directivity_database (Path): Path to the directivity database.
        sources (list[PigeonSource]): List of the sources.
        receivers (list[PigeonReceiver]): List of the receivers.
        reflection_order (int): Max reflection order.
        diffraction_order (int): Max diffraction order.
        combined_order (int): Max combined order.
        result_file (Path): Path to the result json file.
        filter_not_visible_paths (bool): Flag indicating whether to filter not visible paths (i.e. paths that are blocked by geometry).
        export_runtime_statistics (bool): Flag indicating whether to export runtime statistics.
        export_visualization (bool): Flag indicating whether to export visualization.
        runtime_statistics_file (Path): Path to the output runtime statistics file.
        visualization_file (Path): Path to the output visualization file.
        expert_config (Optional[ExpertConfig]): Optional expert configuration.
    """

    geometry_file: Path = Path("geometry.dae")
    material_database: Path = Path(".")
    directivity_database: Path = Path(".")
    sources: list[PigeonSource] = dataclasses.field(default_factory=list)
    receivers: list[PigeonReceiver] = dataclasses.field(default_factory=list)

    reflection_order: int = 1
    diffraction_order: int = 1
    combined_order: int = 1

    result_file: Path = Path("result.json")
    filter_not_visible_paths: bool = True
    export_runtime_statistics: bool = False
    export_visualization: bool = False
    runtime_statistics_file: Path = Path("runtime_statistics.json")
    visualization_file: Path = Path("visualization.dae")

    expert_config: ExpertConfig | None = None
