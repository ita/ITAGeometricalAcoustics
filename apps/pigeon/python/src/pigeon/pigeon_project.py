from __future__ import annotations

import configparser
import re
import shutil
import subprocess
import tkinter as tk
import warnings
from importlib import metadata
from pathlib import Path
from tkinter import filedialog
from typing import Literal

from platformdirs import user_config_dir

from pigeon.pigeon_config import PigeonConfig, PigeonReceiver, PigeonSource

matlab_exe = shutil.which("matlab")


class PigeonProject:
    """Represents a Pigeon project.

    If the executable path is not specified, the Pigeon executable will be searched for in the
    system path. If the executable path is not found, the user will be prompted to specify the
    path to the executable.

    Once the path is found, it will be stored in the user configuration directory for future use.

    All source and receiver position must be given in OpenGL coordinates.

    Note:
        All paths in the configuration are considered relative to the working directory.
        If this is not the desired behavior, the paths should be specified as absolute paths.

        For the geometry file, Blender files are handled separately. If the geometry file is a
        Blender file, the geometry will be exported to a .dae file in the working directory.
        For this, two cases are considered:
            - If an objects with the name "geometry" are found in the Blender file, only those will
                be exported. This check is case-insensitive and "geometry" can be just a part of the
                object name.
            - If no objects with the name "geometry" are found, all mesh objects will be exported.
        Furthermore, sources and receivers can also be specified in the Blender file. Objects with
        names containing "source", "src" or "emitter" will be considered sources and objects with
        names containing "receiver", "rcv" or "sensor" will be considered receivers. Again, this check is
        case insensitive and the identifier can be just part of the name. The position and rotation of
        the objects will be used as the position and rotation of the sources and receivers.
        The directivity of the sources can be specified as a custom string `object property`_
        named "directivity".
        If this is used, it is recommended to specify the geometry object specifically otherwise, the
        sources and receivers will also be used as geometry objects. This however can also be useful
        if the sources and receivers are part of the geometry.

    Args:
        working_dir (Path | str): The working directory for the project.
        executable_path (Path | str | None, optional): The path to the Pigeon executable. Defaults to None.
        config (PigeonConfig, optional): The configuration for the Pigeon project. Defaults to PigeonConfig().

    Raises:
        FileNotFoundError: If the executable path is not found.

    Attributes:
        executable_path (Path): The path to the Pigeon executable.
        config (PigeonConfig): The configuration for the Pigeon project.

    .. _object property:
        https://docs.blender.org/manual/en/latest/files/custom_properties.html
    """

    def __init__(
        self,
        working_dir: Path | str,
        executable_path: Path | str | None = None,
        config: PigeonConfig | None = None,
    ):
        if config is None:
            config = PigeonConfig()

        self._working_dir = Path(working_dir).absolute()
        self.executable_path = executable_path
        self.config = config

        self._working_dir.mkdir(parents=True, exist_ok=True)

        if not self.executable_path.exists():
            msg = f"Executable not found: {self.executable_path}"
            raise FileNotFoundError(msg)

    def run(self):
        """Runs the Pigeon project.

        This method creates a configuration file and executes the Pigeon project
        using the specified executable path and configuration file path.

        Raises:
            RuntimeError: If the execution of the Pigeon project returns a non-zero
                return code.

        Returns:
            None
        """
        self._create_config_file()

        result = subprocess.run(
            [str(self.executable_path), str(self._working_dir / "pigeon.ini")],
            cwd=".",
            capture_output=True,
            check=False,
        )

        if result.returncode != 0:
            raise RuntimeError(result.stdout.decode())

    def create_filter(self, output_file: Path | str, n_bins=2**16 + 1, sampling_rate=44100, diffraction_implementation: Literal["utd", "udfa"]="utd"):
        """Creates a filter from the Pigeon project.

        This method creates a filter from the Pigeon project and saves it to the specified
        output file. The output file can be in .mat, .wav or .ita format.

        Note:
            - This method requires Matlab to be installed and available on the path.
            - The number of bins is and thus the length of the filter needs to be larger than the
              longest propagation delay in the scene.

        Args:
            output_file (Path | str): The output file path.
            n_bins (int, optional): The number of bins for the filter. Defaults to 2**16 + 1.
            sampling_rate (int, optional): The sampling rate for the filter. Defaults to 44100.

        Raises:
            ValueError: If the output file format is not supported.
            RuntimeError: If the execution of the Matlab filter generation returns a non-zero
                return code.
        """
        output_file = Path(output_file)

        if not matlab_exe:
            warnings.warn(
                "Matlab is not available on the path; no filter will be created",
                stacklevel=2,
            )
            return

        if output_file.suffix not in [".mat", ".wav", ".ita"]:
            msg = f"Output file format not supported: {output_file.suffix} must be .mat, .wav or .ita"
            raise ValueError(msg)

        if not output_file.is_absolute():
            output_file = self._working_dir / output_file

        material_database = self.config.material_database
        if not self.config.material_database.is_absolute():
            material_database = self._working_dir / self.config.material_database

        if material_database == output_file.parent and output_file.suffix == ".mat":
            warnings.warn(
                "MAT files should not be in the same directory as the material database",
                stacklevel=2,
            )

        result_file = self.config.result_file
        if not result_file.is_absolute():
            result_file = self._working_dir / result_file

        matlab_script = f"""
prop_engine = which('ITAGeoPropagation');
if isempty(prop_engine)
    error('ITAGeoPropagation not found on the path')
end

prop = itaGeoPropagation({sampling_rate},{n_bins});
prop.diffraction_model = '{diffraction_implementation}';
prop.load_paths_and_materials('{result_file}');

directivity_database_path = '{self.config.directivity_database}';
if ~exist(directivity_database_path, 'dir')
    error('Directivity database not found')
end
directivity_extensions = {{'*.daff', '*.sofa', '*.ita'}};
directivity_files = cellfun(@(x)dir(fullfile(directivity_database_path, ['**' filesep x ])), directivity_extensions, 'UniformOutput', false);
directivity_files = vertcat(directivity_files{{:}});

for i = 1:length(directivity_files)
    prop.load_directivity(fullfile(directivity_files(i).folder, directivity_files(i).name));
end

result = prop.run();

field_names = fieldnames(result);
ita_result(numel(field_names)) = itaAudio;

for idx = 1:numel(field_names)
    ita_result(idx).samplingRate = {sampling_rate};
    ita_result(idx).signalType = 'energy';
    ita_result(idx).freqData = result.(field_names{{idx}});

    for jdx = 1:ita_result(idx).nChannels
        ita_result(idx).channelNames(jdx) = {{[field_names{{idx}} '--Ch:' num2str(jdx)]}};
    end
end

ita_result = merge(ita_result);

output_file = '{output_file}';
[~,~,ext] = fileparts(output_file);
if strcmp(ext, '.mat')
    save(output_file, 'result');
elseif strcmp(ext, '.wav')
    audiowrite(output_file, ita_result.timeData, {sampling_rate}, 'BitsPerSample', 32);
elseif strcmp(ext, '.ita')
    ita_write_ita(ita_result, output_file, 'overwrite');
end
"""

        self.run()
        result = subprocess.run([matlab_exe, "-batch", matlab_script], capture_output=True, check=False)

        if result.returncode != 0:
            msg = f"""Matlab error: {result.stderr.decode()}
========================
Matlab script:
{matlab_script}"""
            raise RuntimeError(msg)

    def _create_config_file(self):
        """Creates a configuration file for the Pigeon project.

        This method creates a configuration file for the Pigeon project using the
        specified configuration and working directory.

        Raises:
            ImportError: If Blender is not installed and a Blender file is used as the geometry file.
            RuntimeError: If a source or receiver name is used more than once.
        """
        config = configparser.ConfigParser()
        config["pigeon:scene"] = {}
        section = config["pigeon:scene"]

        if self.config.geometry_file.suffix == ".blend":
            try:
                import bpy
                import mathutils
            except ImportError as e:
                msg = "Blender files are not supported without Blender installed"
                raise ImportError(msg) from e

            bpy.ops.wm.open_mainfile(filepath=str(self.config.geometry_file))

            if bpy.context.object and bpy.context.object.mode != "OBJECT":
                bpy.ops.object.mode_set(mode="OBJECT")

            bpy.ops.object.select_all(action="DESELECT")

            def get_object_data(obj):
                position = (obj.location[0], obj.location[2], -obj.location[1])
                rotation = None
                if obj.rotation_mode == "QUATERNION":
                    rotation = obj.rotation_quaternion[:]
                else:
                    rotation = mathutils.Euler(
                        obj.rotation_euler[:]
                    ).to_quaternion()[:]

                directivity = obj["directivity"] if "directivity" in obj else ""

                return position, rotation, directivity

            for obj in bpy.data.objects:
                if any(
                    name in obj.name.lower() for name in ["source", "src", "emitter"]
                ):
                    pos, rot, direc = get_object_data(obj)
                    self.config.sources.append(
                        PigeonSource(
                            obj.name, pos, rot, direc
                        )
                    )
                if any(
                    name in obj.name.lower() for name in ["receiver", "rcv", "sensor"]
                ):
                    pos, rot, direc = get_object_data(obj)
                    self.config.receivers.append(
                        PigeonReceiver(
                            obj.name, pos, rot, direc
                        )
                    )

            for obj in bpy.data.objects:
                if "geometry" in obj.name.lower() and obj.type == "MESH":
                    obj.select_set(True)

            selected_objects = bpy.context.selected_objects
            if len(selected_objects) == 0:
                bpy.ops.object.select_all(action="SELECT")

            geometry_path = self._working_dir / "geometry.dae"
            # Note here, the triangulate option is set to False. This reduces errors in the
            # path finding.
            bpy.ops.wm.collada_export(filepath=str(geometry_path), selected=True, triangulate=False)

            section["GeometryFilePath"] = str(geometry_path)
        else:
            section["GeometryFilePath"] = str(self.config.geometry_file)

        result_file = self.config.result_file
        if not result_file.is_absolute():
            result_file = self._working_dir / result_file
        section["OutputFilePath"] = str(result_file)

        material_database = self.config.material_database
        if not material_database.is_absolute():
            material_database = self._working_dir / material_database
        section["MaterialDatabase"] = str(material_database)

        directivity_database = self.config.directivity_database
        if not directivity_database.is_absolute():
            directivity_database = self._working_dir / directivity_database
        section["DirectivityDatabase"] = str(directivity_database)

        source_names = []
        for source in self.config.sources:
            if source.name in source_names:
                msg = f"Source name '{source.name}' used more than once"
                raise RuntimeError(msg)
            source_names.append(source.name)

        receiver_names = []
        for receiver in self.config.receivers:
            if receiver.name in receiver_names:
                msg = f"Receiver name '{receiver.name}' used more than once"
                raise RuntimeError(msg)
            receiver_names.append(receiver.name)

        config["pigeon:scene:emitters"] = {}
        section = config["pigeon:scene:emitters"]
        for source in self.config.sources:
            section[source.name] = ",".join(
                map(
                    str,
                    [
                        source.name,
                        *source.position,
                        *source.rotation,
                        source.directivity,
                    ],
                )
            )

        config["pigeon:scene:sensors"] = {}
        section = config["pigeon:scene:sensors"]
        for receiver in self.config.receivers:
            section[receiver.name] = ",".join(
                map(
                    str,
                    [
                        receiver.name,
                        *receiver.position,
                        *receiver.rotation,
                        receiver.directivity,
                    ],
                )
            )

        config["pigeon:config"] = {}
        section = config["pigeon:config"]

        section["MaxDiffractionOrder"] = str(self.config.diffraction_order)
        section["MaxReflectionOrder"] = str(self.config.reflection_order)
        section["MaxCombinedOrder"] = str(self.config.combined_order)
        section["ExportVisualization"] = str(self.config.export_visualization)

        section["FilterNotVisiblePaths"] = str(self.config.filter_not_visible_paths)
        section["ExportRuntimeStatistics"] = str(self.config.export_runtime_statistics)
        section["RuntimeStatisticsFilePath"] = str(self.config.runtime_statistics_file)

        config["pigeon:visualization"] = {}
        section = config["pigeon:visualization"]

        visualization_file_path = self.config.visualization_file
        if not visualization_file_path.is_absolute():
            visualization_file_path = self._working_dir / visualization_file_path

        section["VisualizationFilePath"] = str(visualization_file_path)

        if self.config.visualization_file.suffix == ".dae":
            section["VisualizationFileFormat"] = "collada"

        if self.config.expert_config:
            section = config["pigeon:config"]

            section["LevelDropThreshold"] = str(self.config.expert_config.level_drop_threshold)
            section["ReflectionPenalty"] = str(self.config.expert_config.reflection_penalty)
            section["DiffractionPenalty"] = str(self.config.expert_config.diffraction_penalty)
            section["MaxAccumulatedDiffractionAngle"] = str(self.config.expert_config.max_accumulated_diffraction_angle)

            section["OnlyNeighboredEdgeDiffraction"] = str(self.config.expert_config.only_neighbored_edge_diffraction)
            section["DiffractionOnlyIntoShadowedEdges"] = str(
                self.config.expert_config.diffraction_only_into_shadowed_edges
            )
            section["FilterNotVisiblePathsBetweenEdges"] = str(
                self.config.expert_config.filter_not_visible_paths_between_edges
            )
            section["FilterEmitterToEdgeIntersectedPaths"] = str(
                self.config.expert_config.filter_emitter_to_edge_intersected_paths
            )
            section["FilterSensorToEdgeIntersectedPaths"] = str(
                self.config.expert_config.filter_sensor_to_edge_intersected_paths
            )
            section["IntersectionTestResolution"] = str(self.config.expert_config.intersection_test_resolution)
            section["NumIterations"] = str(self.config.expert_config.max_apex_calculation_iteration)

        with open(self._working_dir / "pigeon.ini", "w") as config_file:
            config.write(config_file)

    @property
    def executable_path(self) -> Path:
        """The path to the Pigeon executable."""
        return self._executable_path

    @executable_path.setter
    def executable_path(self, value: Path | str | None = None):
        """Sets the path to the Pigeon executable.

        If the path is not specified, the Pigeon executable will be searched for in the system path.
        If the path is not found, the user will be prompted to specify the path to the executable.

        Once the path is found, it will be stored in the user configuration directory for future use.

        Args:
            value (Path | str | None, optional): The path to the Pigeon executable. Defaults to None.

        Raises:
            FileNotFoundError: If the executable path is not found.
        """
        if value is None:
            config_dir = Path(user_config_dir("pigeon_project", version=metadata.version("pigeon")))

            executable = None
            if (config_dir / "pigeon_project.cfg").exists():
                config = configparser.ConfigParser()
                config.read(config_dir / "pigeon_project.cfg")
                executable = Path(config["executable"]["path"])

            if executable is None or not executable.is_file() or not executable.exists():
                executable = shutil.which("pigeon")
                if executable is None:
                    root = tk.Tk()
                    root.withdraw()

                    executable = filedialog.askopenfilename(
                        title="Specify pigeon.exe",
                        initialdir=".",
                        filetypes=[("executable", "exe")],
                    )

                    if not executable:
                        msg = "No executable specified"
                        raise FileNotFoundError(msg)

                result = subprocess.run([str(executable), "--version"], capture_output=True, check=False)
                exe_version = result.stdout.decode().strip()
                exe_version = re.split(r"v(\d{4})(\D)", exe_version)
                major_version = exe_version[1]
                minor_version = exe_version[2]
                minor_version = ord(minor_version) - 97
                exe_version = f"{major_version}.{minor_version}"

                # This should never happen as the --version flag was introduced in v2024a, but better safe than sorry
                if int(major_version) < 2024:
                    msg = f"Unsupported Pigeon version: {exe_version}. Minimum version required: v2024a"
                    raise ValueError(msg)

                if metadata.version("pigeon") != exe_version:
                    warnings.warn(
                        f"Version mismatch between Pigeon python package and Pigeon executable. Python package version: {metadata.version('pigeon')}, executable version: {exe_version}",
                        stacklevel=2,
                    )

                config = configparser.ConfigParser()
                config["executable"] = {"path": str(executable)}
                config_dir.mkdir(parents=True, exist_ok=True)
                with open(config_dir / "pigeon_project.cfg", "w") as config_file:
                    config.write(config_file)

            self._executable_path = Path(executable)

        else:
            self._executable_path = Path(value)
