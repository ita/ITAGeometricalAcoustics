#include "pigeon_window.h"

#include <QApplication>
#include <QMainWindow>

int main_gui( int argc, char *argv[] )
{
	QApplication a( argc, argv );
	a.setOrganizationName( "Institute of Technical Acoustics" );
	a.setOrganizationDomain( "akustik.rwth-aachen.de" );
	a.setApplicationName( "pigeon" );
	a.setApplicationDisplayName( PIGEON_VERSION );

	CPigeonWindow w;
	w.show( );

	return a.exec( );
}
