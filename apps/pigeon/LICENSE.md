## License

The pigeon source code and binaries are licensed under [Apache License Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).

It makes use of the open source version of the [Qt libraries](http://www.qt.io), the [ViSTA core libs](https://www.vr.rwth-aachen.de/software/ViSTA/), [libsndfile](http://www.mega-nerd.com/libsndfile/), [SPLINE](https://people.sc.fsu.edu/~jburkardt/c_src/spline/spline.html) and works with the [OpenMesh data format](https://www.graphics.rwth-aachen.de/software/openmesh/), which are shared under LGPL terms. Additionally, it has a dependency to [libsamplerate](http://www.mega-nerd.com/SRC/)  and the [SketchUp API](https://extensions.sketchup.com/developers/sketchup_c_api/sketchup/index.html).
