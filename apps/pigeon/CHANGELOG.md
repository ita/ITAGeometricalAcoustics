# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to a versioning scheme similar to Matlab, with versions based on the release year.

## [Unreleased]

## [2024a] - 2024-09-26

### Added

- Better exception and error handling and logging (!5)
- Support for multiple source and receiver positions (!6)
- Output "new" propagation path format (!7)
- Python interface for pigeon, incl. "generate_filter" via Matlab (!8)
- Get scene definition from blender in python interface (!10)
- Python interface: improve directivity support (!11)
- Python interface: add option to specify diffraction implementation (!12)
- Python interface: allow for more directivity file formats (!17)

### Changed

- Major rework of the geometry handling (!4) ([ITAGeo!3](https://git.rwth-aachen.de/ita/ITAGeo/-/merge_requests/3))
  - From now on, all source and receiver positions MUST be given in OpenGL coordinates.
  - Materials are now handled by pigeon and are stored in the path list.
- Directivities are now not loaded anymore and are just passed through to the path list (!17)
- README update (!17)

### Fixed

- Relative config file paths (!5)

## [2021a]

- Several bugfixes now allow a proper usage of the ITA-Toolbox interface classes.

## [2020b]

- Initial release used in the paper ["The image edge model"](https://doi.org/10.1051/aacus/2021010).
