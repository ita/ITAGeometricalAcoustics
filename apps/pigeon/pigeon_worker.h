#ifndef IW_PIGEON_WORKER
#define IW_PIGEON_WORKER

#include <ITABase/ITAProgress.h>
#include <ITAGeo/ResourceManager.h>
#include <ITAPropagationPathSim/CombinedModel/PropagationEngine.h>
#include <QObject>

class CPigeonWorker
    : public QObject
    , public ITABase::IProgressHandler
{
	Q_OBJECT

public:
	inline CPigeonWorker( ) : ITABase::IProgressHandler( "Pigeon worker" ) {};
	virtual inline ~CPigeonWorker( ) {};
	void PushStatus( const std::string&, float );

	ITAPropagationPathSim::CombinedModel::CPathEngine::CSimulationConfig oConf;
	ITAPropagationPathSim::CombinedModel::CPathEngine::CAbortionCriteria oAbort;
	std::shared_ptr<ITAGeo::CResourceManager> pResourceManager;
	std::string sGeoFilePath, sOutFilePath, sVizFilePath, sAdditionalPropPaths, sLayerName;
	std::shared_ptr<ITAGeo::CEmitter> pEmitter;
	std::shared_ptr<ITAGeo::CSensor> pSensor;

public slots:
	void RunSimulation( );

signals:
	void UpdateProgress( QString, float ) const;
	void Guru( float );
	void ErrorMessage( QString );

private:
	void PushProgressUpdate( float fProgressPercentage ) const;
};

#endif // IW_PIGEON_WORKER
