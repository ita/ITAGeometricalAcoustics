#include "pigeon_worker.h"

#include <ITAException.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAGeo/Model.h>
#include <ITAGeo/SketchUp/Materials.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITAStopWatch.h>
#include <QErrorMessage>
#include <QString>
#include <VistaBase/VistaExceptionBase.h>
#include <assimp/postprocess.h>
#include <iostream>

void CPigeonWorker::RunSimulation( )
{
	try
	{
		ITAStopWatch sw;
		sw.start( );

		ITAPropagationPathSim::CombinedModel::CPathEngine oSim;
		oSim.SetSimulationConfiguration( oConf );
		oSim.SetAbortionCriteria( oAbort );
		oSim.SetProgressCallbackHandler( this );

		ITAGeo::CModel oModel;
		oModel.SetMaterialManager( pResourceManager->GetMaterialManager( ) );
		emit UpdateProgress( "Loading geometry mesh (might take a while)", 0.0f );
		oModel.Load( sGeoFilePath );
		oSim.InitializePathEnvironment( oModel.GetOpenMesh( ) );
		oSim.SetEntities( pEmitter, pSensor );


		ITAGeo::CPropagationPathList oPathList;
		oSim.ConstructPropagationPaths( oPathList );
		ITAGeoUtils::AddIdentifier( oPathList );

		if( !sVizFilePath.empty( ) )
		{
			auto emitters = std::vector<ITAGeo::CEmitter> { *pEmitter };
			auto sensors  = std::vector<ITAGeo::CSensor> { *pSensor };
			oModel.AddVisualization( oPathList, emitters, sensors );

			std::string sFormat( "" );
			std::filesystem::path fsPath( sVizFilePath );
			if( fsPath.extension( ).string( ).substr( 1 ) == "dae" )
			{
				sFormat = "collada";
			}

			oModel.Store( sVizFilePath, sFormat );
		}

		if( !sOutFilePath.empty( ) )
		{
			emit UpdateProgress( "Exporting propagation paths", 0.0f );
			ITAGeo::Utils::JSON::Export( oPathList, sOutFilePath, true, pResourceManager );
			emit UpdateProgress( "Exporting propagation paths", 100.0f );
		}

		// if( !sVizFilePath.empty( ) )
		//{
		//	SetSection( "Exporting visualization" );

		//	ITABase::IProgressHandler::PushProgressUpdate( "Loading copy of input geometry (might take a while)", 0.0f );
		//	ITAGeo::SketchUp::CModel oGeoModel_Viz;
		//	oGeoModel_Viz.Load( sGeoFilePath );

		//	ITABase::IProgressHandler::PushProgressUpdate( "Adding visualizations", 25.0f );
		//	oGeoModel_Viz.AddEmitterVisualization( *pEmitter, pEmitter->sName );
		//	oGeoModel_Viz.AddSensorVisualization( *pSensor, pSensor->sName );

		//	std::vector<std::vector<size_t> > numberOfPaths;
		//	numberOfPaths.resize( oAbort.iMaxDiffractionOrder + 1 );
		//	for( auto& vi: numberOfPaths )
		//		vi.resize( oAbort.iMaxReflectionOrder + 1 );

		//	for( auto& oPath: oPathList )
		//	{
		//		long iNumReflections  = (long)oPath.GetNumReflections( );
		//		long iNumDiffractions = (long)oPath.GetNumDiffractions( );

		//		numberOfPaths[iNumDiffractions][iNumReflections]++;
		//	}

		//	for( auto& oPath: oPathList )
		//	{
		//		long iNumReflections  = (long)oPath.GetNumReflections( );
		//		long iNumDiffractions = (long)oPath.GetNumDiffractions( );

		//		std::string sPathName = "Refl_Order_" + std::to_string( iNumReflections ) + "_Diffr_Order_" + std::to_string( iNumDiffractions ) + "_Path_Amount_" +
		//		                        std::to_string( numberOfPaths[iNumDiffractions][iNumReflections] );
		//		oGeoModel_Viz.AddPropagationPathVisualization( oPath, sPathName );
		//	}

		//	if( sAdditionalPropPaths.empty( ) == false )
		//	{
		//		std::cout << "Adding propagation paths from file '" << sAdditionalPropPaths << "' to the model under layer name '" << sLayerName << "'" << std::endl;
		//		ITAGeo::CPropagationPathList oAddPropPathList;
		//		ITAGeo::Utils::JSON::Import( oAddPropPathList, sAdditionalPropPaths );
		//		for( const auto& oAddPropPath: oAddPropPathList )
		//			oGeoModel_Viz.AddPropagationPathVisualization( oAddPropPath, sLayerName );
		//	}

		//	ITABase::IProgressHandler::PushProgressUpdate( "Storing output file, might take a while", 50.0f );
		//	oGeoModel_Viz.Store( sVizFilePath );
		//}

		emit Guru( sw.stop( ) );
	}
	catch( ITAException& e )
	{
		QString sError = "Caught an ITAException: " + QString::fromStdString( e.ToString( ) );
		emit ErrorMessage( sError );
	}
	catch( VistaExceptionBase& e )
	{
		QString sError = "Caught a VistaException: " + QString::fromStdString( e.GetBacktraceString( ) );
		emit ErrorMessage( sError );
	}
}

void CPigeonWorker::PushProgressUpdate( float fProgress ) const
{
	QString qsMessage = QString( "%1 (%2)" ).arg( QString::fromStdString( GetSection( ) ) ).arg( QString::fromStdString( GetItem( ) ) );
	emit UpdateProgress( qsMessage, fProgress );
}
