#include "convert_to_ppl.hpp"
#include "pigeon_instrumentation.h"

#include <ITABase/ITAStatistics.h>
#include <ITABase/UtilsJSON.h>
#include <ITAConfigUtils.h>
#include <ITAException.h>
#include <ITAFileSystemUtils.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Directivity/DirectivityManager.h>
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAGeo/Material/MaterialManager.h>
#include <ITAGeo/Model.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Utils.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITAPropagationPathSim/CombinedModel/PropagationEngine.h>
#include <ITAStringUtils.h>
#include <VistaBase/VistaVector3D.h>

// STL
#include <cassert>
#include <filesystem>
#include <iostream>


using namespace std;


// this totally depends on the default width of the console but i dont care. Kinda for debug only anyways
class ITA_BASE_API CConsoleProgressHandler : public ITABase::IProgressHandler
{
	static const int barWidth    = 20;
	mutable size_t oldPos        = 0;
	mutable std::string lastItem = "";

	void PushProgressUpdate( float fProgressPercentage ) const
	{
		// mostly from stackoverflow

		if( lastItem != this->GetItem( ) )
		{
			std::string clearString( 80, ' ' );
			std::cout << clearString << "\r";
			std::cout.flush( );

			std::string bar( barWidth, ' ' );
			std::cout << "[" << bar << "] " << std::setfill( '0' ) << std::setw( 2 ) << std::right << int( fProgressPercentage * 100.0 )
			          << "% - Item: " << std::setfill( ' ' ) << std::setw( 65 ) << std::left << this->GetItem( ) << "\r";
			std::cout.flush( );
			lastItem = this->GetItem( );
			oldPos   = 0;
		}

		fProgressPercentage /= 100;
		size_t pos = barWidth * fProgressPercentage;

		if( oldPos == pos )
			return;


		oldPos = pos;

		// if( fProgressPercentage > 1 )
		//{
		//	// remove progress bar, + some more for good measure
		//	for( int i = 0; i < barWidth + 100; i++ )
		//	{
		//		std::cout << " ";
		//	}
		//	std::cout << "\r";
		//	return;
		// }

		std::cout << "[";
		for( int i = 0; i < barWidth; ++i )
		{
			if( i < pos )
				std::cout << "#";
			else if( i == pos )
				std::cout << ">";
			else
				std::cout << " ";
		}

		std::cout << "] " << std::setfill( '0' ) << std::setw( 2 ) << std::right << int( fProgressPercentage * 100.0 ) << "% - Item: " << std::setfill( ' ' )
		          << std::setw( 65 ) << std::left << this->GetItem( ) << "\r";
		std::cout.flush( );
	}
};


struct CDummyDirectivity : public ITAGeo::Directivity::IDirectivity
{
	CDummyDirectivity( std::string t_name )
	{
		SetName( t_name );
		SetFormat( ITAGeo::Directivity::IDirectivity::Format::NONE );
		SetDomain( ITADomain::ITA_UNKNOWN_DOMAIN );
	}

	int GetNumChannels( ) const override { return 0; }
};


int main_cli( const std::string& config, bool quite, const std::string& logFile, bool new_ppl_format )
{
	IHTA::Instrumentation::LoggerRegistry::DefaultSinksConfig log_config;
	log_config.console_sink_level = quite ? spdlog::level::level_enum::err : spdlog::level::level_enum::info;
	log_config.log_file_name      = logFile;
	IHTA::Instrumentation::LoggerRegistry::create_default_sinks( log_config );

	// ini file found?
	string sInFilePath( config );
	// create cannonical path, relative to absolute path
	std::filesystem::path fsPath = std::filesystem::canonical( std::filesystem::path( sInFilePath ) );
	if( !std::filesystem::exists( fsPath ) )
	{
		PIGEON_CRITICAL( "Input file '{}' not found.", fsPath.string( ) );
		return 255;
	}
	if( !std::filesystem::is_regular_file( fsPath ) )
	{
		PIGEON_CRITICAL( "Input '{}' not a file.", fsPath.filename( ).string( ) );
		return 255;
	}

	// set current filesystem path. All relative files are relative to the position of the .ini file. Not the pigeon executable!
	std::filesystem::current_path( fsPath.parent_path( ) );
	sInFilePath = fsPath.string( );

	/// Variables for config --------------------------------------------------------------------------------------------------------------

	// Scene
	string sOutFilePath, sGeometryFilePath, sVisualizationPath, sStatsOutFilePath;
	VistaVector3D v3EmitterPos;
	VistaVector3D v3SensorPos;
	std::vector<ITAGeo::CEmitter> vEmitters;
	std::vector<ITAGeo::CSensor> vSensors;
	auto pModel = std::make_shared<ITAGeo::CModel>( );

	std::shared_ptr<ITAGeo::Material::CMaterialManager> pMaterialManager( new ITAGeo::Material::CMaterialManager );
	std::shared_ptr<ITAGeo::Directivity::CDirectivityManager> pDirectivityManager( new ITAGeo::Directivity::CDirectivityManager );
	std::shared_ptr<ITAGeo::CResourceManager> pResourceManager = std::make_shared<ITAGeo::CResourceManager>( pMaterialManager, pDirectivityManager );

	// Config
	ITAPropagationPathSim::CombinedModel::CPathEngine::CSimulationConfig oSimConfig; // Auto-default
	ITAPropagationPathSim::CombinedModel::CPathEngine::CAbortionCriteria oSimAbort;  // Auto-default

	bool bExportViz;
	std::string sExportFormat;


	///-----------------------------------------------------------------------------------------------------------------------------------
	///----Parse config-------------------------------------------------------------------------------------------------------------------
	///-----------------------------------------------------------------------------------------------------------------------------------
	try
	{
		INIFileUseFile( sInFilePath );
		if( !INIFileUseSectionIfExists( "pigeon:scene" ) )
			INIFileUseSection( "pigeon" );


		PIGEON_DEBUG( "Load databases if available" );

		const std::string sMaterialDatabasePath    = INIFileReadStringExplicit( "MaterialDatabase" );
		const std::string sDirectivityDatabasePath = INIFileReadStringExplicit( "DirectivityDatabase" );

		pMaterialManager->AddMaterialsFromFolder( sMaterialDatabasePath );
	}
	catch( ITAException& e )
	{
		PIGEON_CRITICAL( "Error while reading databases from config: {}", e.ToString( ) );
		return 255;
	}

	try
	{
		PIGEON_DEBUG( "Reload because materialManager may overwrite current iniFile for material load" );
		INIFileUseFile( sInFilePath );
		if( !INIFileUseSectionIfExists( "pigeon:scene" ) )
			INIFileUseSection( "pigeon" );


		/// ----------------------------------------------------------------
		PIGEON_DEBUG( "Geometry, Output, Sensors and Emitters" );

		PIGEON_DEBUG( "Geometry" );
		sGeometryFilePath = INIFileReadStringExplicit( "GeometryFilePath" );

		PIGEON_INFO( "Geometry input file path: {}", sGeometryFilePath );

		PIGEON_DEBUG( "Output" );
		sOutFilePath = INIFileReadString( "OutputFilePath", fsPath.parent_path( ).string( ) + "\\" + fsPath.stem( ).string( ) + "_out.json" );

		PIGEON_INFO( "Output file path: {}", sOutFilePath );

		PIGEON_DEBUG( "Emitters" );
		if( !INIFileUseSectionIfExists( "pigeon:scene:emitters" ) )
			ITA_EXCEPT_INVALID_PARAMETER( "'pigeon:scene:emitters' not found. Please create section with Emitter arguments." );
		for( const auto& sEmitter: INIFileGetKeys( "pigeon:scene:emitters" ) )
		{
			ITAGeo::CEmitter oEmitter;
			std::string sEmitterData                   = INIFileReadStringExplicit( sEmitter );
			std::vector<std::string> vEmitterArguments = splitString( sEmitterData, "," );
			std::transform( vEmitterArguments.cbegin( ), vEmitterArguments.cend( ), vEmitterArguments.begin( ), stripSpaces );

			if( vEmitterArguments.size( ) != 4 && vEmitterArguments.size( ) != 5 && vEmitterArguments.size( ) != 8 && vEmitterArguments.size( ) != 9 )
				ITA_EXCEPT_INVALID_PARAMETER(
				    "Emitter requires 4 arguments (name, x, y, z) Orientation (x, y, z, w) or Directivity (identifier) are optional. Dont use a ',' in the name." )

			if( vEmitterArguments.size( ) == 5 || vEmitterArguments.size( ) == 9 )
			{
				const int iDirectivityIndex = vEmitterArguments.size( ) == 5 ? 4 : 8; // is orientation given? directivity always at end
				if( !vEmitterArguments.at( iDirectivityIndex ).empty( ) )
				{
					oEmitter.pDirectivity = std::make_shared<CDummyDirectivity>( vEmitterArguments.at( iDirectivityIndex ) );
				}
			}
			if( vEmitterArguments.size( ) == 8 || vEmitterArguments.size( ) == 9 )
			{
				// orientation given
				VistaQuaternion qEmitterOrient;
				qEmitterOrient[Vista::X] = std::stof( vEmitterArguments.at( 4 ) );
				qEmitterOrient[Vista::Y] = std::stof( vEmitterArguments.at( 5 ) );
				qEmitterOrient[Vista::Z] = std::stof( vEmitterArguments.at( 6 ) );
				qEmitterOrient[Vista::W] = std::stof( vEmitterArguments.at( 7 ) );
				oEmitter.qOrient         = qEmitterOrient;
			}
			oEmitter.sName = vEmitterArguments.at( 0 );
			VistaVector3D v3EmitterPos;
			v3EmitterPos[Vista::X]      = std::stof( vEmitterArguments.at( 1 ) );
			v3EmitterPos[Vista::Y]      = std::stof( vEmitterArguments.at( 2 ) );
			v3EmitterPos[Vista::Z]      = std::stof( vEmitterArguments.at( 3 ) );
			oEmitter.v3InteractionPoint = v3EmitterPos;
			oEmitter.vPos               = v3EmitterPos; // i dont know why this exist but set it just in case

			vEmitters.push_back( oEmitter );
		}

		PIGEON_INFO( "Emitter position(s): " );
		for( const auto& oEmitter: vEmitters )
			PIGEON_INFO( "    {}", oEmitter.ToString( ) );

		PIGEON_DEBUG( "Sensors" );
		if( !INIFileUseSectionIfExists( "pigeon:scene:sensors" ) )
			ITA_EXCEPT_INVALID_PARAMETER( "'pigeon:scene:sensors' not found. Please create section with Sensor arguments." );
		for( const auto& sSensor: INIFileGetKeys( "pigeon:scene:sensors" ) )
		{
			ITAGeo::CSensor oSensor;
			std::string sSensorData                   = INIFileReadStringExplicit( sSensor );
			std::vector<std::string> vSensorArguments = splitString( sSensorData, "," );
			std::transform( vSensorArguments.cbegin( ), vSensorArguments.cend( ), vSensorArguments.begin( ), stripSpaces );

			if( vSensorArguments.size( ) != 4 && vSensorArguments.size( ) != 5 && vSensorArguments.size( ) != 8 && vSensorArguments.size( ) != 9 )
				ITA_EXCEPT_INVALID_PARAMETER(
				    "Sensor requires 4 arguments (name, x, y, z) Orientation (x, y, z, w) or Directivity (identifier) are optional. Dont use a ',' in the name." )

			if( vSensorArguments.size( ) == 5 || vSensorArguments.size( ) == 9 )
			{
				const int iDirectivityIndex = vSensorArguments.size( ) == 5 ? 4 : 8; // is orientation given? directivity always at end
				if( !vSensorArguments.at( iDirectivityIndex ).empty( ) )
				{
					oSensor.pDirectivity = std::make_shared<CDummyDirectivity>( vSensorArguments.at( iDirectivityIndex ) );
				}
			}
			if( vSensorArguments.size( ) == 8 || vSensorArguments.size( ) == 9 )
			{
				// orientation given
				VistaQuaternion qSensorOrient;
				qSensorOrient[Vista::X] = std::stof( vSensorArguments.at( 4 ) );
				qSensorOrient[Vista::Y] = std::stof( vSensorArguments.at( 5 ) );
				qSensorOrient[Vista::Z] = std::stof( vSensorArguments.at( 6 ) );
				qSensorOrient[Vista::W] = std::stof( vSensorArguments.at( 7 ) );
				oSensor.qOrient         = qSensorOrient;
			}
			oSensor.sName = vSensorArguments.at( 0 );
			VistaVector3D v3SensorPos;
			v3SensorPos[Vista::X]      = std::stof( vSensorArguments.at( 1 ) );
			v3SensorPos[Vista::Y]      = std::stof( vSensorArguments.at( 2 ) );
			v3SensorPos[Vista::Z]      = std::stof( vSensorArguments.at( 3 ) );
			oSensor.v3InteractionPoint = v3SensorPos;
			oSensor.vPos               = v3SensorPos; // i dont know why this exist but set it just in case

			vSensors.push_back( oSensor );
		}

		PIGEON_INFO( "Sensor position(s): " );
		for( const auto& oSensor: vSensors )
			PIGEON_INFO( "    {}", oSensor.ToString( ) );

		if( vEmitters.size( ) == 0 || vSensors.size( ) == 0 )
			ITA_EXCEPT_INVALID_PARAMETER( "At least one Emitter and one Sensor is necessary!" );
	}
	catch( ITAException& e )
	{
		PIGEON_CRITICAL( "Error while reading config for geometry, output, sensors and emitters: {}", e.ToString( ) );
		return 255;
	}

	try
	{
		/// ---------------------------------------------------------------------------------------------------------
		PIGEON_DEBUG( "Get simulation Config" );
		if( !INIFileUseSectionIfExists( "pigeon:config" ) )
			INIFileUseSection( "pigeon" );

		oSimAbort.iMaxDiffractionOrder       = INIFileReadInt( "MaxDiffractionOrder", oSimAbort.iMaxDiffractionOrder );
		oSimAbort.iMaxReflectionOrder        = INIFileReadInt( "MaxReflectionOrder", oSimAbort.iMaxReflectionOrder );
		oSimAbort.iMaxCombinedOrder          = INIFileReadInt( "MaxCombinedOrder", oSimAbort.iMaxCombinedOrder );
		oSimAbort.fDynamicRange              = INIFileReadFloat( "LevelDropThreshold", oSimAbort.fDynamicRange );
		oSimAbort.fReflectionPenalty         = INIFileReadFloat( "ReflectionPenalty", oSimAbort.fReflectionPenalty );
		oSimAbort.fDiffractionPenalty        = INIFileReadFloat( "DiffractionPenalty", oSimAbort.fDiffractionPenalty );
		oSimAbort.fAccumulatedAngleThreshold = INIFileReadFloat( "MaxAccumulatedDiffractionAngle", oSimAbort.fAccumulatedAngleThreshold );

		oSimConfig.bFilterNotNeighbouredEdges           = INIFileReadBool( "OnlyNeighbouredEdgeDiffraction", oSimConfig.bFilterNotNeighbouredEdges );
		oSimConfig.bFilterIlluminatedRegionDiffraction  = INIFileReadBool( "DiffractionOnlyIntoShadowedEdges", oSimConfig.bFilterIlluminatedRegionDiffraction );
		oSimConfig.bFilterEdgeToEdgeIntersectedPaths    = INIFileReadBool( "FilterNotVisiblePathsBetweenEdges", oSimConfig.bFilterEdgeToEdgeIntersectedPaths );
		oSimConfig.bFilterEmitterToEdgeIntersectedPaths = INIFileReadBool( "FilterEmitterToEdgeIntersectedPaths", oSimConfig.bFilterEmitterToEdgeIntersectedPaths );
		oSimConfig.bFilterSensorToEdgeIntersectedPaths  = INIFileReadBool( "FilterSensorToEdgeIntersectedPaths", oSimConfig.bFilterSensorToEdgeIntersectedPaths );
		oSimConfig.bFilterIntersectedPaths              = INIFileReadBool( "FilterNotVisiblePaths", oSimConfig.bFilterIntersectedPaths );
		oSimConfig.fIntersectionTestResolution          = INIFileReadFloat( "IntersectionTestResolution", oSimConfig.fIntersectionTestResolution );
		oSimConfig.iNumberIterationApexCalculation      = INIFileReadInt( "NumIterations", oSimConfig.iNumberIterationApexCalculation );
		oSimConfig.bExportRuntimeStatistics             = INIFileReadBool( "ExportRuntimeStatistics", oSimConfig.bExportRuntimeStatistics );

		// Where to write stats if exportRuntimeStatistics is true
		sStatsOutFilePath = INIFileReadString( "RuntimeStatisticsFilePath", fsPath.parent_path( ).string( ) + "\\" + fsPath.stem( ).string( ) + "_stats.json" );
	}
	catch( ITAException& e )
	{
		PIGEON_CRITICAL( "Error while reading config for simulation config: {}", e.ToString( ) );
		return 255;
	}

	try
	{
		/// -----------------------------------------------------------------------------------------------------------
		PIGEON_DEBUG( "Get Visualization" );
		bExportViz = INIFileReadBool( "ExportVisualization", false );

		if( !INIFileUseSectionIfExists( "pigeon:visualization" ) )
			INIFileUseSection( "pigeon" );

		// lets just check for visualisation flag again in case someone puts flag into visualisation section instead of config
		bExportViz = INIFileReadBool( "ExportVisualization", bExportViz );

		if( bExportViz )
		{
			sVisualizationPath = INIFileReadStringExplicit( "VisualizationFilePath" );
			if( !std::filesystem::exists( std::filesystem::path( sVisualizationPath ).parent_path( ) ) )
				ITA_EXCEPT_INVALID_PARAMETER( "Visualization file path does not exist." );

			sExportFormat = INIFileReadString( "VisualizationFileFormat", "" ); // NMK: assimp sometimes needs specific fileformat, cant derive from file ending
		}
	}
	catch( ITAException& e )
	{
		PIGEON_CRITICAL( "Error while reading config for visualization: {}", e.ToString( ) );
		return 255;
	}

	///---------------------------------------------------------------------------------------------------------------------------------
	///---Load Geometry ----------------------------------------------------------------------------------------------------------------
	///---------------------------------------------------------------------------------------------------------------------------------

	PIGEON_INFO( "Attempting to load geometry '{}'", sGeometryFilePath );

	pModel->SetMaterialManager( pMaterialManager );
	try
	{
		bool bSuccess = pModel->Load( sGeometryFilePath );

		if( bSuccess )
		{
			PIGEON_INFO( "Successfully loaded geometry mesh" );
		}
		else
		{
			PIGEON_CRITICAL( "Could not load geometry mesh from path '{}'", sGeometryFilePath );
			return 255;
		}
	}
	catch( ITAException& e )
	{
		PIGEON_CRITICAL( "Error while loading geometry: {}", e.ToString( ) );
		return 255;
	}
	catch( ... )
	{
		PIGEON_CRITICAL( "An unkown error ocurred while loading the geometry file '{}'", sGeometryFilePath );
		return 255;
	}

	///-------------------------------------------------------------------------------------------------------------------------------
	///---Simulation------------------------------------------------------------------------------------------------------------------
	///-------------------------------------------------------------------------------------------------------------------------------

	PIGEON_INFO( "Propagation path algorithm configuration:" );
	PIGEON_INFO( "    Maximum reflection order  : {}", oSimAbort.iMaxReflectionOrder );
	PIGEON_INFO( "    Maximum diffraction order : {}", oSimAbort.iMaxDiffractionOrder );
	PIGEON_INFO( "    Maximum combined order    : {}", oSimAbort.iMaxCombinedOrder );

	// Path finding
	ITAPropagationPathSim::CombinedModel::CPathEngine oPathEngine;
	ITABase::IProgressHandler* progressHandler = new CConsoleProgressHandler( );
	oPathEngine.SetProgressCallbackHandler( progressHandler );
	oPathEngine.SetSimulationConfiguration( oSimConfig );
	oPathEngine.SetAbortionCriteria( oSimAbort );
	try
	{
		oPathEngine.InitializePathEnvironment( pModel->GetOpenMesh( ) );
	}
	catch( const ITAException& e )
	{
		PIGEON_CRITICAL( "Couldn't get openmesh: {}", e.ToString( ) );
		return 255;
	}

	if( oSimConfig.bExportRuntimeStatistics && ( vSensors.size( ) > 1 || vEmitters.size( ) > 1 ) )
	{
		PIGEON_WARN( "Runtime statistics are only exported for the first emitter-sensor pairs." );
	}

	ITAGeo::CPropagationPathList oPathList;
	for( const auto& emitter: vEmitters )
	{
		for( const auto& sensor: vSensors )
		{
			PIGEON_INFO( "Simulating: Emitter: {}, Sensor: {}", emitter.sName, sensor.sName );
			ITAGeo::CPropagationPathList tmpPathList;
			oPathEngine.SetEntities( std::make_shared<ITAGeo::CEmitter>( emitter ), std::make_shared<ITAGeo::CSensor>( sensor ) );

			// actual simulation
			oPathEngine.ConstructPropagationPaths( tmpPathList );

			PIGEON_INFO( "==> found {} visible paths", tmpPathList.size( ) );

			oPathList.insert( oPathList.end( ), tmpPathList.begin( ), tmpPathList.end( ) );

			if( vSensors.size( ) > 1 || vEmitters.size( ) > 1 )
			{
				continue;
			}

			if( oSimConfig.bExportRuntimeStatistics )
			{
				PIGEON_INFO( "Runtime statistics output file path: ", sStatsOutFilePath );
				std::vector<ITABase::CStatistics> voStats = oPathEngine.GetRuntimeStatistics( );
				ITABase::Utils::JSON::Export( voStats, sStatsOutFilePath );
			}
		}
	}

	// Add Identifier Names to each path
	ITAGeoUtils::AddIdentifier( oPathList );

	///-----------------------------------------------------------------------------------------------------------------------------
	///---Export--------------------------------------------------------------------------------------------------------------------
	///-----------------------------------------------------------------------------------------------------------------------------

	/// Propagation path list export -----------------------------------------------------------------------------------------------
	if( new_ppl_format )
	{
		auto ppl = convert_to_ppl( oPathList, pResourceManager );

		std::ofstream output_stream( sOutFilePath );
		cereal::JSONOutputArchive archive( output_stream );
		ppl.serialize( archive );
	}
	else
	{
		PIGEON_WARN( "This export format is deprecated and will be removed in the next version. Please use the new format via the '--ppl' switch" );
		ITAGeo::Utils::JSON::Export( oPathList, sOutFilePath, true, pResourceManager );
	}

	PIGEON_INFO( "Exported propagation path list to '{}'", sOutFilePath );

	/// Export visualization ------------------------------------------------------------------------------------------------------

	if( bExportViz )
	{
		PIGEON_INFO( "Attempting to export visualization to '{}'", sVisualizationPath );

		pModel->AddVisualization( oPathList, vEmitters, vSensors );
		bool bExportSuccess = false;
		try
		{
			bExportSuccess = pModel->Store( sVisualizationPath, sExportFormat );
		}
		catch( ITAException& e )
		{
			PIGEON_CRITICAL( "Couldn't store visualization: {}", e.ToString( ) );
			return 255;
		}

		if( bExportSuccess )
		{
			PIGEON_INFO( "Successfully exported visualization file list to '{}'", sVisualizationPath );
		}
		else
		{
			PIGEON_ERROR( "Failed to store visualization. Is the VisualizationFileFormat set correctly?" );
		}
	}

	return 0;
}
