#include "pigeon_window.h"

#include <ITAException.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITANumericUtils.h>
#include <ITAStringUtils.h>
#include <QErrorMessage>
#include <QFileDialog>
#include <QThread>
#include <QWidget>
#include <VistaBase/VistaExceptionBase.h>
#include <VistaBase/VistaVector3D.h>
#include <ui_pigeon_window.h>

using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationPathSim;

class CDummyDirectivitiy : public ITAGeo::Directivity::IDirectivity
{
public:
	inline CDummyDirectivitiy( std::string sName )
	{
		SetName( sName );
		SetFormat( ITAGeo::Directivity::IDirectivity::NONE );
	};

	inline int GetNumChannels( ) const { return 0; };
};

CPigeonWindow::CPigeonWindow( QWidget* parent ) : QMainWindow( parent ), ui( new Ui::CPigeonWindow ), m_pWorker( nullptr )
{
	ui->setupUi( this );

	m_pWorkerThread = new QThread( this );

	LoadSettings( );
}

CPigeonWindow::~CPigeonWindow( )
{
	delete m_pWorkerThread;
	delete m_pWorker;
}

// This is the most interesting function
void CPigeonWindow::on_pushButton_run_clicked( )
{
	SetGUIElementsEnabled( false );
	StoreSettings( ); // ... keep curret settings in case app crashes or pocess is killed by user
	ui->statusBar->showMessage( "Loading configuration" );

	auto pDummyResourceManager = make_shared<ITAGeo::Directivity::CDirectivityManager>( );


	auto pEmitter   = make_shared<ITAGeo::CEmitter>( );
	pEmitter->sName = ui->lineEdit_emitter_id->text( ).toStdString( );

	VistaVector3D& v3EmitterPos( pEmitter->v3InteractionPoint );
	v3EmitterPos[Vista::X] = float( ui->doubleSpinBox_emitter_pos_x->value( ) );
	v3EmitterPos[Vista::Y] = float( ui->doubleSpinBox_emitter_pos_y->value( ) );
	v3EmitterPos[Vista::Z] = float( ui->doubleSpinBox_emitter_pos_z->value( ) );

	VistaQuaternion& qEmitterOrient( pEmitter->qOrient );
	qEmitterOrient[Vista::X] = float( ui->doubleSpinBox_emitter_quat_x->value( ) );
	qEmitterOrient[Vista::Y] = float( ui->doubleSpinBox_emitter_quat_y->value( ) );
	qEmitterOrient[Vista::Z] = float( ui->doubleSpinBox_emitter_quat_z->value( ) );
	qEmitterOrient[Vista::W] = float( ui->doubleSpinBox_emitter_quat_w->value( ) );

	if( !ui->lineEdit_emitter_directivity_id->text( ).isEmpty( ) )
	{
		pEmitter->pDirectivity = make_shared<CDummyDirectivitiy>( "EmitterDummyDirectivity" );
		pDummyResourceManager->AddDirectivity( ui->lineEdit_emitter_directivity_id->text( ).toStdString( ), pEmitter->pDirectivity );
	}


	auto pSensor   = make_shared<ITAGeo::CSensor>( );
	pSensor->sName = ui->lineEdit_sensor_id->text( ).toStdString( );

	VistaVector3D& v3SensorPos( pSensor->v3InteractionPoint );
	v3SensorPos[Vista::X] = float( ui->doubleSpinBox_sensor_pos_x->value( ) );
	v3SensorPos[Vista::Y] = float( ui->doubleSpinBox_sensor_pos_y->value( ) );
	v3SensorPos[Vista::Z] = float( ui->doubleSpinBox_sensor_pos_z->value( ) );

	VistaQuaternion& qSensorOrient( pSensor->qOrient );
	qSensorOrient[Vista::X] = float( ui->doubleSpinBox_sensor_quat_x->value( ) );
	qSensorOrient[Vista::Y] = float( ui->doubleSpinBox_sensor_quat_y->value( ) );
	qSensorOrient[Vista::Z] = float( ui->doubleSpinBox_sensor_quat_z->value( ) );
	qSensorOrient[Vista::W] = float( ui->doubleSpinBox_sensor_quat_w->value( ) );

	if( !ui->lineEdit_sensor_directivity_id->text( ).isEmpty( ) )
	{
		pSensor->pDirectivity = make_shared<CDummyDirectivitiy>( "SensorDummyDirectivity" );
		pDummyResourceManager->AddDirectivity( ui->lineEdit_sensor_directivity_id->text( ).toStdString( ), pSensor->pDirectivity );
	}


	CombinedModel::CPathEngine::CSimulationConfig oConf;
	CombinedModel::CPathEngine::CAbortionCriteria oAbort;

	if( ui->groupBox_config->isChecked( ) )
	{
		oAbort.iMaxCombinedOrder = ui->spinBox_max_combined_order->value( );
		oAbort.fDynamicRange     = ui->doubleSpinBox_dynamic_range->value( );

		if( ui->groupBox_config_reflection->isChecked( ) )
		{
			oAbort.iMaxReflectionOrder = ui->spinBox_reflection_order->value( );
			oAbort.fReflectionPenalty  = ui->doubleSpinBox_reflection_penalty->value( );
		}

		if( ui->groupBox_config_diffraction->isChecked( ) )
		{
			oAbort.iMaxDiffractionOrder = ui->spinBox_diffraction_order->value( );
			oAbort.fDiffractionPenalty  = ui->doubleSpinBox_diffraction_penalty->value( );
			if( ui->checkBox_diffraction_accumulated_angle->isChecked( ) )
				oAbort.fAccumulatedAngleThreshold = ui->doubleSpinBox_diffraction_accumulated_angle->value( );
			oConf.bFilterIlluminatedRegionDiffraction = !ui->checkBox_diffraction_back_scattering->isChecked( );
			oConf.bFilterNotNeighbouredEdges          = ui->checkBox_diffraction_adjacent->isChecked( );
			oConf.fIntersectionTestResolution         = (float)ui->doubleSpinBox_diffraction_intersection_res->value( );
			oConf.iNumberIterationApexCalculation     = ui->spinBox_diffraction_iterations->value( );
		}

		if( ui->groupBox_config_intersection_tests->isChecked( ) )
		{
			oConf.bFilterIntersectedPaths              = true;
			oConf.bFilterEdgeToEdgeIntersectedPaths    = ui->checkBox_intersection_intra_path->isChecked( );
			oConf.bFilterEmitterToEdgeIntersectedPaths = ui->checkBox_intersection_emitter_vicinity->isChecked( );
			oConf.bFilterSensorToEdgeIntersectedPaths  = ui->checkBox_intersection_sensor_vicinity->isChecked( );
		}
	}

	m_pWorker         = new CPigeonWorker( );
	m_pWorker->oConf  = oConf;
	m_pWorker->oAbort = oAbort;

	auto pMaterialManager = std::make_shared<ITAGeo::Material::CMaterialManager>( ui->lineEdit_mat_database->text( ).toStdString( ), false );

	m_pWorker->pResourceManager = std::make_shared<ITAGeo::CResourceManager>( pMaterialManager, pDummyResourceManager );

	m_pWorker->pEmitter = pEmitter;
	m_pWorker->pSensor  = pSensor;

	m_pWorker->sGeoFilePath = ui->lineEdit_geo_input->text( ).toStdString( );
	if( ui->groupBox_propagation_paths->isChecked( ) )
		m_pWorker->sOutFilePath = ui->lineEdit_pp_export->text( ).toStdString( );
	else
		m_pWorker->sOutFilePath.clear( );

	if( ui->groupBox_visualization->isChecked( ) )
	{
		m_pWorker->sVizFilePath   = ui->lineEdit_viz_geo_output->text( ).toStdString( );
		auto sAdditionalPropPaths = ui->lineEdit_viz_add_paths->text( ).toStdString( );
		if( !sAdditionalPropPaths.empty( ) )
		{
			m_pWorker->sAdditionalPropPaths = sAdditionalPropPaths;
			m_pWorker->sLayerName           = ui->lineEdit_viz_add_layer_name->text( ).toStdString( );
		}
		else
		{
			m_pWorker->sAdditionalPropPaths.clear( );
			m_pWorker->sLayerName.clear( );
		}
	}
	else
	{
		m_pWorker->sVizFilePath.clear( );
	}

	m_pWorker->moveToThread( m_pWorkerThread );

	connect( m_pWorkerThread, SIGNAL( started( ) ), m_pWorker, SLOT( RunSimulation( ) ) );
	connect( m_pWorker, SIGNAL( Guru( float ) ), this, SLOT( StopWorkerThread( float ) ) );
	connect( m_pWorker, SIGNAL( UpdateProgress( QString, float ) ), this, SLOT( UpdateProgress( QString, float ) ) );
	connect( m_pWorker, SIGNAL( ErrorMessage( QString ) ), this, SLOT( ErrorMessage( QString ) ) );

	m_pWorkerThread->start( ); // thread will take control of workflow emit updates & terminate when done.
}

void CPigeonWindow::SetGUIElementsEnabled( bool bEnabled )
{
	ui->progressBar_run->setEnabled( !bEnabled );
	ui->groupBox_scene->setEnabled( bEnabled );
	ui->groupBox_config->setEnabled( bEnabled );
	ui->groupBox_visualization->setEnabled( bEnabled );
	ui->pushButton_run->setEnabled( bEnabled );
}

void CPigeonWindow::StopWorkerThread( float fRuntime )
{
	m_pWorkerThread->terminate( );
	delete m_pWorker;
	m_pWorker = nullptr;

	auto qsMsg = QString( "Guru (all done) in %1." ).arg( QString::fromStdString( timeToString( fRuntime ) ) );
	emit CPigeonWindow::UpdateProgress( qsMsg, 100.0f );
	SetGUIElementsEnabled( true );
}

void CPigeonWindow::ErrorMessage( QString sError )
{
	emit StopWorkerThread( 0.0f );
	emit CPigeonWindow::UpdateProgress( "Gurks (error).", 100.0f );

	QErrorMessage qe;
	qe.showMessage( sError );
	qe.exec( );
}

void CPigeonWindow::closeEvent( QCloseEvent* event )
{
	StoreSettings( );
	QMainWindow::closeEvent( event );

	m_pWorkerThread->terminate( );
	m_pWorkerThread->wait( );
}

void CPigeonWindow::on_actionQuit_triggered( )
{
	close( );
}

void CPigeonWindow::LoadSettings( )
{
	ui->lineEdit_geo_input->setText( m_qSettings.value( "scene/geo_input_file_path" ).toString( ) );
	ui->lineEdit_mat_database->setText( m_qSettings.value( "scene/mat_database_file_path" ).toString( ) );

	ui->doubleSpinBox_emitter_pos_x->setValue( m_qSettings.value( "scene/emitter/pos/x" ).toDouble( ) );
	ui->doubleSpinBox_emitter_pos_y->setValue( m_qSettings.value( "scene/emitter/pos/y" ).toDouble( ) );
	ui->doubleSpinBox_emitter_pos_z->setValue( m_qSettings.value( "scene/emitter/pos/z" ).toDouble( ) );

	ui->doubleSpinBox_emitter_quat_x->setValue( m_qSettings.value( "scene/emitter/quat/x" ).toDouble( ) );
	ui->doubleSpinBox_emitter_quat_y->setValue( m_qSettings.value( "scene/emitter/quat/y" ).toDouble( ) );
	ui->doubleSpinBox_emitter_quat_z->setValue( m_qSettings.value( "scene/emitter/quat/z" ).toDouble( ) );
	ui->doubleSpinBox_emitter_quat_w->setValue( m_qSettings.value( "scene/emitter/quat/w" ).toDouble( ) );

	ui->lineEdit_emitter_id->setText( m_qSettings.value( "scene/emitter/id" ).toString( ) );
	ui->lineEdit_emitter_directivity_id->setText( m_qSettings.value( "scene/emitter/directivity/id" ).toString( ) );

	ui->doubleSpinBox_sensor_pos_x->setValue( m_qSettings.value( "scene/sensor/pos/x" ).toDouble( ) );
	ui->doubleSpinBox_sensor_pos_y->setValue( m_qSettings.value( "scene/sensor/pos/y" ).toDouble( ) );
	ui->doubleSpinBox_sensor_pos_z->setValue( m_qSettings.value( "scene/sensor/pos/z" ).toDouble( ) );

	ui->doubleSpinBox_sensor_quat_x->setValue( m_qSettings.value( "scene/sensor/quat/x" ).toDouble( ) );
	ui->doubleSpinBox_sensor_quat_y->setValue( m_qSettings.value( "scene/sensor/quat/y" ).toDouble( ) );
	ui->doubleSpinBox_sensor_quat_z->setValue( m_qSettings.value( "scene/sensor/quat/z" ).toDouble( ) );
	ui->doubleSpinBox_sensor_quat_w->setValue( m_qSettings.value( "scene/sensor/quat/w" ).toDouble( ) );

	ui->lineEdit_sensor_id->setText( m_qSettings.value( "scene/sensor/id" ).toString( ) );
	ui->lineEdit_sensor_directivity_id->setText( m_qSettings.value( "scene/sensor/directivity/id" ).toString( ) );

	ui->lineEdit_pp_export->setText( m_qSettings.value( "scene/propagation_path/file" ).toString( ) );

	ui->groupBox_visualization->setChecked( m_qSettings.value( "visualization" ).toBool( ) );
	ui->lineEdit_viz_geo_output->setText( m_qSettings.value( "viz/geo_output_file_path" ).toString( ) );
	ui->lineEdit_viz_add_paths->setText( m_qSettings.value( "viz/add/path" ).toString( ) );
	ui->lineEdit_viz_add_layer_name->setText( m_qSettings.value( "scene/add/name" ).toString( ) );

	ui->groupBox_config->setChecked( m_qSettings.value( "config" ).toBool( ) );
	ui->doubleSpinBox_dynamic_range->setValue( m_qSettings.value( "config/dynamic_range" ).toDouble( ) );
	ui->spinBox_max_combined_order->setValue( m_qSettings.value( "config/max_combined_order" ).toInt( ) );

	ui->groupBox_config_reflection->setChecked( m_qSettings.value( "config/reflection" ).toBool( ) );
	ui->spinBox_reflection_order->setValue( m_qSettings.value( "config/reflection/order" ).toInt( ) );
	ui->doubleSpinBox_reflection_penalty->setValue( m_qSettings.value( "config/reflection/penalty" ).toDouble( ) );

	ui->groupBox_config_diffraction->setChecked( m_qSettings.value( "config/diffraction" ).toBool( ) );
	ui->spinBox_diffraction_order->setValue( m_qSettings.value( "config/diffraction/order" ).toInt( ) );
	ui->doubleSpinBox_diffraction_penalty->setValue( m_qSettings.value( "config/diffraction/penalty" ).toDouble( ) );
	ui->doubleSpinBox_diffraction_intersection_res->setValue( m_qSettings.value( "config/diffraction/intersection/res" ).toDouble( ) );
	ui->spinBox_diffraction_iterations->setValue( m_qSettings.value( "config/diffraction/intersection/iterations" ).toInt( ) );
	ui->checkBox_diffraction_adjacent->setChecked( m_qSettings.value( "config/diffraction/adjacent" ).toBool( ) );
	ui->checkBox_diffraction_back_scattering->setChecked( m_qSettings.value( "config/diffraction/backscattering" ).toBool( ) );
	ui->checkBox_diffraction_accumulated_angle->setChecked( m_qSettings.value( "config/diffraction/accumulated_angle" ).toBool( ) );
	ui->doubleSpinBox_diffraction_accumulated_angle->setValue( m_qSettings.value( "config/diffraction/accumulated_angle_val" ).toDouble( ) );

	ui->groupBox_config_intersection_tests->setChecked( m_qSettings.value( "config/intersection" ).toBool( ) );
	ui->checkBox_intersection_emitter_vicinity->setChecked( m_qSettings.value( "config/intersection/emitter_vicinity" ).toBool( ) );
	ui->checkBox_intersection_sensor_vicinity->setChecked( m_qSettings.value( "config/intersection/sensor_vicinity" ).toBool( ) );
	ui->checkBox_intersection_intra_path->setChecked( m_qSettings.value( "config/intersection/intra_path" ).toBool( ) );
}

void CPigeonWindow::StoreSettings( )
{
	m_qSettings.setValue( "scene/geo_input_file_path", ui->lineEdit_geo_input->text( ) );
	m_qSettings.setValue( "scene/mat_database_file_path", ui->lineEdit_mat_database->text( ) );

	m_qSettings.setValue( "scene/emitter/pos/x", ui->doubleSpinBox_emitter_pos_x->value( ) );
	m_qSettings.setValue( "scene/emitter/pos/y", ui->doubleSpinBox_emitter_pos_y->value( ) );
	m_qSettings.setValue( "scene/emitter/pos/z", ui->doubleSpinBox_emitter_pos_z->value( ) );
	m_qSettings.setValue( "scene/emitter/quat/x", ui->doubleSpinBox_emitter_quat_x->value( ) );
	m_qSettings.setValue( "scene/emitter/quat/y", ui->doubleSpinBox_emitter_quat_y->value( ) );
	m_qSettings.setValue( "scene/emitter/quat/z", ui->doubleSpinBox_emitter_quat_z->value( ) );
	m_qSettings.setValue( "scene/emitter/quat/w", ui->doubleSpinBox_emitter_quat_w->value( ) );
	m_qSettings.setValue( "scene/emitter/id", ui->lineEdit_emitter_id->text( ) );
	m_qSettings.setValue( "scene/emitter/directivity/id", ui->lineEdit_emitter_directivity_id->text( ) );

	m_qSettings.setValue( "scene/sensor/pos/x", ui->doubleSpinBox_sensor_pos_x->value( ) );
	m_qSettings.setValue( "scene/sensor/pos/y", ui->doubleSpinBox_sensor_pos_y->value( ) );
	m_qSettings.setValue( "scene/sensor/pos/z", ui->doubleSpinBox_sensor_pos_z->value( ) );
	m_qSettings.setValue( "scene/sensor/quat/x", ui->doubleSpinBox_sensor_quat_x->value( ) );
	m_qSettings.setValue( "scene/sensor/quat/y", ui->doubleSpinBox_sensor_quat_y->value( ) );
	m_qSettings.setValue( "scene/sensor/quat/z", ui->doubleSpinBox_sensor_quat_z->value( ) );
	m_qSettings.setValue( "scene/sensor/quat/w", ui->doubleSpinBox_sensor_quat_w->value( ) );
	m_qSettings.setValue( "scene/sensor/id", ui->lineEdit_sensor_id->text( ) );
	m_qSettings.setValue( "scene/sensor/directivity/id", ui->lineEdit_sensor_directivity_id->text( ) );

	m_qSettings.setValue( "scene/propagation_path/file", ui->lineEdit_pp_export->text( ) );

	m_qSettings.setValue( "visualization", ui->groupBox_visualization->isChecked( ) );
	m_qSettings.setValue( "viz/geo_output_file_path", ui->lineEdit_viz_geo_output->text( ) );
	m_qSettings.setValue( "viz/add/path", ui->lineEdit_viz_add_paths->text( ) );
	m_qSettings.setValue( "scene/add/name", ui->lineEdit_viz_add_layer_name->text( ) );

	m_qSettings.setValue( "config", ui->groupBox_config->isChecked( ) );
	m_qSettings.setValue( "config/dynamic_range", ui->doubleSpinBox_dynamic_range->value( ) );
	m_qSettings.setValue( "config/max_combined_order", ui->spinBox_max_combined_order->value( ) );

	m_qSettings.setValue( "config/reflection", ui->groupBox_config_reflection->isChecked( ) );
	m_qSettings.setValue( "config/reflection/order", ui->spinBox_reflection_order->value( ) );
	m_qSettings.setValue( "config/reflection/penalty", ui->doubleSpinBox_reflection_penalty->value( ) );

	m_qSettings.setValue( "config/diffraction", ui->groupBox_config_diffraction->isChecked( ) );
	m_qSettings.setValue( "config/diffraction/order", ui->spinBox_diffraction_order->value( ) );
	m_qSettings.setValue( "config/diffraction/penalty", ui->doubleSpinBox_diffraction_penalty->value( ) );
	m_qSettings.setValue( "config/diffraction/intersection/res", ui->doubleSpinBox_diffraction_intersection_res->value( ) );
	m_qSettings.setValue( "config/diffraction/intersection/iterations", ui->spinBox_diffraction_iterations->value( ) );
	m_qSettings.setValue( "config/diffraction/adjacent", ui->checkBox_diffraction_adjacent->isChecked( ) );
	m_qSettings.setValue( "config/diffraction/backscattering", ui->checkBox_diffraction_back_scattering->isChecked( ) );
	m_qSettings.setValue( "config/diffraction/accumulated_angle", ui->checkBox_diffraction_accumulated_angle->isChecked( ) );
	m_qSettings.setValue( "config/diffraction/accumulated_angle_val", ui->doubleSpinBox_diffraction_accumulated_angle->value( ) );

	m_qSettings.setValue( "config/intersection", ui->groupBox_config_intersection_tests->isChecked( ) );
	m_qSettings.setValue( "config/intersection/emitter_vicinity", ui->checkBox_intersection_emitter_vicinity->isChecked( ) );
	m_qSettings.setValue( "config/intersection/sensor_vicinity", ui->checkBox_intersection_sensor_vicinity->isChecked( ) );
	m_qSettings.setValue( "config/intersection/intra_path", ui->checkBox_intersection_intra_path->isChecked( ) );
}

void CPigeonWindow::Reset( )
{
	ui->lineEdit_geo_input->clear( );
	ui->lineEdit_mat_database->clear( );

	ui->doubleSpinBox_emitter_pos_x->setValue( 0.0f );
	ui->doubleSpinBox_emitter_pos_y->setValue( 0.0f );
	ui->doubleSpinBox_emitter_pos_z->setValue( 0.0f );
	ui->doubleSpinBox_emitter_quat_x->setValue( 0.0f );
	ui->doubleSpinBox_emitter_quat_y->setValue( 0.0f );
	ui->doubleSpinBox_emitter_quat_z->setValue( 0.0f );
	ui->doubleSpinBox_emitter_quat_w->setValue( 1.0f );

	ui->lineEdit_emitter_id->setText( "PigeonEmitter" );
	ui->lineEdit_emitter_directivity_id->setText( "" );

	ui->doubleSpinBox_sensor_pos_x->setValue( 0.0f );
	ui->doubleSpinBox_sensor_pos_y->setValue( 0.0f );
	ui->doubleSpinBox_sensor_pos_z->setValue( 0.0f );
	ui->doubleSpinBox_sensor_quat_x->setValue( 0.0f );
	ui->doubleSpinBox_sensor_quat_y->setValue( 0.0f );
	ui->doubleSpinBox_sensor_quat_z->setValue( 0.0f );
	ui->doubleSpinBox_sensor_quat_w->setValue( 1.0f );

	ui->lineEdit_sensor_id->setText( "PigeonSensor" );
	ui->lineEdit_sensor_directivity_id->setText( "" );

	ui->groupBox_propagation_paths->setChecked( true );
	ui->lineEdit_pp_export->clear( );

	ui->groupBox_visualization->setChecked( false );
	ui->lineEdit_viz_geo_output->clear( );
	ui->lineEdit_viz_add_paths->clear( );
	ui->lineEdit_viz_add_layer_name->clear( );

	CombinedModel::CPathEngine::CSimulationConfig& oDefaultConfig( m_oSimConf );
	oDefaultConfig.SetDefaults( );

	CombinedModel::CPathEngine::CAbortionCriteria oDefaultAbort( m_oSimAbort );
	oDefaultAbort.SetDefaults( );

	ui->groupBox_config->setChecked( false );
	ui->doubleSpinBox_dynamic_range->setValue( ratio_to_db20( oDefaultAbort.fDynamicRange ) );
	ui->spinBox_max_combined_order->setValue( oDefaultAbort.iMaxCombinedOrder );

	ui->groupBox_config_reflection->setChecked( true );
	ui->spinBox_reflection_order->setValue( oDefaultAbort.iMaxReflectionOrder );
	ui->doubleSpinBox_reflection_penalty->setValue( ratio_to_db20( oDefaultAbort.fReflectionPenalty ) );

	ui->groupBox_config_diffraction->setChecked( true );
	ui->spinBox_diffraction_order->setValue( oDefaultAbort.iMaxDiffractionOrder );
	ui->doubleSpinBox_diffraction_penalty->setValue( ratio_to_db20( oDefaultAbort.fDiffractionPenalty ) );
	ui->doubleSpinBox_diffraction_intersection_res->setValue( oDefaultConfig.fIntersectionTestResolution );
	ui->spinBox_diffraction_iterations->setValue( oDefaultConfig.iNumberIterationApexCalculation );
	ui->checkBox_diffraction_adjacent->setChecked( oDefaultConfig.bFilterNotNeighbouredEdges );
	ui->checkBox_diffraction_back_scattering->setChecked( !oDefaultConfig.bFilterIlluminatedRegionDiffraction );
	ui->checkBox_diffraction_accumulated_angle->setChecked( bool( oDefaultAbort.fAccumulatedAngleThreshold > 0.0f ) );
	ui->doubleSpinBox_diffraction_accumulated_angle->setValue( oDefaultAbort.fAccumulatedAngleThreshold );

	ui->groupBox_config_intersection_tests->setChecked( oDefaultConfig.bFilterIntersectedPaths );
	ui->checkBox_intersection_emitter_vicinity->setChecked( oDefaultConfig.bFilterEmitterToEdgeIntersectedPaths );
	ui->checkBox_intersection_sensor_vicinity->setChecked( oDefaultConfig.bFilterSensorToEdgeIntersectedPaths );
	ui->checkBox_intersection_intra_path->setChecked( oDefaultConfig.bFilterEdgeToEdgeIntersectedPaths );
}

void CPigeonWindow::UpdateProgress( QString sMsg, float fProgress )
{
	ui->statusBar->showMessage( sMsg );
	ui->progressBar_run->setValue( (int)fProgress );
}

void CPigeonWindow::on_actionReset_triggered( )
{
	Reset( );
}

void CPigeonWindow::on_pushButton_geo_input_clicked( )
{
	QFileDialog fd;
	fd.setNameFilter( "SketchUp files (*.skp)" );
	fd.setViewMode( QFileDialog::Detail );
	fd.setFileMode( QFileDialog::ExistingFile );

	QDir oOpenDialogLastDirectory( m_qSettings.value( "InputFileBrowserDialogLastDirectory" ).toString( ) );
	if( oOpenDialogLastDirectory.exists( ) )
		fd.setDirectory( oOpenDialogLastDirectory );
	else
		fd.setDirectory( QDir( QApplication::applicationDirPath( ) ) );

	if( fd.exec( ) )
	{
		QStringList lFiles = fd.selectedFiles( );
		if( lFiles.empty( ) == false )
			ui->lineEdit_geo_input->setText( lFiles[0] );

		QString sOpenDialogLastDirectory = fd.directory( ).absolutePath( );
		m_qSettings.setValue( "InputFileBrowserDialogLastDirectory", sOpenDialogLastDirectory );
	}
}

void CPigeonWindow::on_pushButton_mat_database_clicked( )
{
	QFileDialog fd;
	fd.setNameFilter( "Material database" );
	fd.setViewMode( QFileDialog::Detail );
	fd.setFileMode( QFileDialog::Directory );

	QDir oOpenDialogLastDirectory( m_qSettings.value( "InputFileBrowserDialogLastDirectory" ).toString( ) );
	if( oOpenDialogLastDirectory.exists( ) )
		fd.setDirectory( oOpenDialogLastDirectory );
	else
		fd.setDirectory( QDir( QApplication::applicationDirPath( ) ) );

	if( fd.exec( ) )
	{
		QStringList lFiles = fd.selectedFiles( );
		if( lFiles.empty( ) == false )
			ui->lineEdit_mat_database->setText( lFiles[0] );

		QString sOpenDialogLastDirectory = fd.directory( ).absolutePath( );
		m_qSettings.setValue( "InputFileBrowserDialogLastDirectory", sOpenDialogLastDirectory );
	}
}

void CPigeonWindow::on_pushButton_pp_export_clicked( )
{
	QFileDialog fd;
	fd.setNameFilter( "JSON files (*.json)" );
	fd.setViewMode( QFileDialog::Detail );
	fd.setFileMode( QFileDialog::AnyFile );

	QDir oOpenDialogLastDirectory( m_qSettings.value( "ExportPPFileBrowserDialogLastDirectory" ).toString( ) );
	if( oOpenDialogLastDirectory.exists( ) )
		fd.setDirectory( oOpenDialogLastDirectory );
	else
		fd.setDirectory( QDir( QApplication::applicationDirPath( ) ) );

	if( fd.exec( ) )
	{
		QStringList lFiles = fd.selectedFiles( );
		if( lFiles.empty( ) == false )
			ui->lineEdit_pp_export->setText( lFiles[0] );

		QString sOpenDialogLastDirectory = fd.directory( ).absolutePath( );
		m_qSettings.setValue( "ExportPPFileBrowserDialogLastDirectory", sOpenDialogLastDirectory );
	}
}

void CPigeonWindow::on_pushButton_viz_geo_output_clicked( )
{
	QFileDialog fd;
	fd.setNameFilter( "SketchUp files (*.obj *.dae)" );
	fd.setViewMode( QFileDialog::Detail );
	fd.setFileMode( QFileDialog::AnyFile );

	QDir oOpenDialogLastDirectory( m_qSettings.value( "ExportGeoVizFileBrowserDialogLastDirectory" ).toString( ) );
	if( oOpenDialogLastDirectory.exists( ) )
		fd.setDirectory( oOpenDialogLastDirectory );
	else
		fd.setDirectory( QDir( QApplication::applicationDirPath( ) ) );

	if( fd.exec( ) )
	{
		QStringList lFiles = fd.selectedFiles( );
		if( lFiles.empty( ) == false )
			ui->lineEdit_viz_geo_output->setText( lFiles[0] );

		QString sOpenDialogLastDirectory = fd.directory( ).absolutePath( );
		m_qSettings.setValue( "ExportGeoVizFileBrowserDialogLastDirectory", sOpenDialogLastDirectory );
	}
}

void CPigeonWindow::on_pushButton_viz_add_paths_clicked( )
{
	QFileDialog fd;
	fd.setNameFilter( "JSON files (*.json)" );
	fd.setViewMode( QFileDialog::Detail );
	fd.setFileMode( QFileDialog::ExistingFile );

	QDir oOpenDialogLastDirectory( m_qSettings.value( "VizAddPathsFileBrowserDialogLastDirectory" ).toString( ) );
	if( oOpenDialogLastDirectory.exists( ) )
		fd.setDirectory( oOpenDialogLastDirectory );
	else
		fd.setDirectory( QDir( QApplication::applicationDirPath( ) ) );

	if( fd.exec( ) )
	{
		QStringList lFiles = fd.selectedFiles( );
		if( lFiles.empty( ) == false )
			ui->lineEdit_viz_add_paths->setText( lFiles[0] );

		QString sOpenDialogLastDirectory = fd.directory( ).absolutePath( );
		m_qSettings.setValue( "VizAddPathsFileBrowserDialogLastDirectory", sOpenDialogLastDirectory );
	}
}
