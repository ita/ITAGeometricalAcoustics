#include <ITAGeo/Base.h>
#include <ITAGeo/Material/Material.h>
#include <propagation_path_list/propagation_path_list.hpp>

inline std::shared_ptr<propagation_path_list::PropagationAnchorBase> convert_anchor_to_ppl( const std::shared_ptr<ITAGeo::CPropagationAnchor>& anchor )
{
	if( auto emitter = std::dynamic_pointer_cast<ITAGeo::CEmitter>( anchor ) )
	{
		auto source_ppl               = std::make_shared<propagation_path_list::Source>( );
		source_ppl->interaction_point = { emitter->v3InteractionPoint[0], emitter->v3InteractionPoint[1], emitter->v3InteractionPoint[2] };
		source_ppl->orientation       = { emitter->qOrient[0], emitter->qOrient[1], emitter->qOrient[2], emitter->qOrient[3] };
		source_ppl->name              = emitter->sName;
		if( emitter->pDirectivity )
		{
			source_ppl->directivity_id = emitter->pDirectivity->GetName( );
		}
		return source_ppl;
	}
	else if( auto sensor = std::dynamic_pointer_cast<ITAGeo::CSensor>( anchor ) )
	{
		auto receiver_ppl               = std::make_shared<propagation_path_list::Receiver>( );
		receiver_ppl->interaction_point = { sensor->v3InteractionPoint[0], sensor->v3InteractionPoint[1], sensor->v3InteractionPoint[2] };
		receiver_ppl->orientation       = { sensor->qOrient[0], sensor->qOrient[1], sensor->qOrient[2], sensor->qOrient[3] };
		receiver_ppl->name              = sensor->sName;
		if( sensor->pDirectivity )
		{
			receiver_ppl->directivity_id = sensor->pDirectivity->GetName( );
		}
		return receiver_ppl;
	}
	else if( auto specular_reflection = std::dynamic_pointer_cast<ITAGeo::CSpecularReflection>( anchor ) )
	{
		auto specular_reflection_ppl               = std::make_shared<propagation_path_list::SpecularReflection>( );
		specular_reflection_ppl->interaction_point = { specular_reflection->v3InteractionPoint[0], specular_reflection->v3InteractionPoint[1],
			                                           specular_reflection->v3InteractionPoint[2] };
		specular_reflection_ppl->face_normal       = { specular_reflection->v3FaceNormal[0], specular_reflection->v3FaceNormal[1], specular_reflection->v3FaceNormal[2] };
		specular_reflection_ppl->polygon_id        = specular_reflection->iPolygonID;
		if( specular_reflection->pMaterial )
		{
			specular_reflection_ppl->material_id = specular_reflection->pMaterial->GetIdentifier( );
		}
		return specular_reflection_ppl;
	}
	else if( auto edge_diffraction = std::dynamic_pointer_cast<ITAGeo::CITADiffractionOuterWedgeAperture>( anchor ) )
	{
		auto edge_diffraction_ppl                        = std::make_shared<propagation_path_list::EdgeDiffraction>( );
		edge_diffraction_ppl->interaction_point          = { edge_diffraction->v3InteractionPoint[0], edge_diffraction->v3InteractionPoint[1],
			                                                 edge_diffraction->v3InteractionPoint[2] };
		edge_diffraction_ppl->main_wedge_face_normal     = { edge_diffraction->v3MainWedgeFaceNormal[0], edge_diffraction->v3MainWedgeFaceNormal[1],
			                                                 edge_diffraction->v3MainWedgeFaceNormal[2] };
		edge_diffraction_ppl->opposite_wedge_face_normal = { edge_diffraction->v3OppositeWedgeFaceNormal[0], edge_diffraction->v3OppositeWedgeFaceNormal[1],
			                                                 edge_diffraction->v3OppositeWedgeFaceNormal[2] };
		edge_diffraction_ppl->main_wedge_face_id         = edge_diffraction->iMainWedgeFaceID;
		edge_diffraction_ppl->opposite_wedge_face_id     = edge_diffraction->iOppositeWedgeFaceID;
		edge_diffraction_ppl->vertex_start = { edge_diffraction->v3VertextStart[0], edge_diffraction->v3VertextStart[1], edge_diffraction->v3VertextStart[2] };
		edge_diffraction_ppl->vertex_end   = { edge_diffraction->v3VertextEnd[0], edge_diffraction->v3VertextEnd[1], edge_diffraction->v3VertextEnd[2] };
		return edge_diffraction_ppl;
	}
	else
	{
		throw std::runtime_error( "Unknown anchor type" );
	}
}

inline propagation_path_list::PropagationPathList convert_to_ppl( const ITAGeo::CPropagationPathList& paths, std::shared_ptr<ITAGeo::CResourceManager> resourceManager )
{
	propagation_path_list::PropagationPathList ppl;
	ppl.propagation_paths.reserve( paths.size( ) );

	std::vector<std::string> material_ids;

	for( auto&& path: paths )
	{
		ppl.propagation_paths.emplace_back( );
		auto& ppl_path = ppl.propagation_paths.back( );

		ppl_path.identifier = path.sIdentifier;

		ppl_path.propagation_anchors.reserve( path.size( ) );

		for( auto&& anchor: path )
		{
			const auto ppl_anchor = convert_anchor_to_ppl( anchor );

			if( auto reflection = std::dynamic_pointer_cast<propagation_path_list::SpecularReflection>( ppl_anchor ) )
			{
				if( auto material = resourceManager->GetMaterial( reflection->material_id ) )
				{
					material_ids.push_back( material->GetIdentifier( ) );
				}
			}

			ppl_path.propagation_anchors.push_back( std::move( ppl_anchor ) );
		}
	}

	for( auto&& material_id: material_ids )
	{
		auto material = resourceManager->GetMaterial( material_id );
		if( material )
		{
			if( auto third_octave_material = std::dynamic_pointer_cast<ITAGeo::Material::CThirdOctaveMaterial>( material ) )
			{
				auto ppl_material = std::make_shared<propagation_path_list::ThirdOctaveMaterial>( );

				auto third_octave_to_ppl = []( const std::vector<float>& from, std::array<double, 31>& to )
				{
					if( from.size( ) != 31 )
					{
						throw std::runtime_error( "Only 31 bands are supported" );
					}

					std::copy( from.begin( ), from.end( ), to.begin( ) );
				};

				third_octave_to_ppl( third_octave_material->oAbsorptionCoefficients.GetValues( ), ppl_material->absorption );
				third_octave_to_ppl( third_octave_material->oScatteringCoefficients.GetValues( ), ppl_material->scattering );
				third_octave_to_ppl( third_octave_material->vfImpedanceRealCoefficients, ppl_material->impedance_real );
				third_octave_to_ppl( third_octave_material->vfImpedanceImagCoefficients, ppl_material->impedance_imag );

				ppl.material_database[material_id] = ppl_material;
			}
			if( auto scalar_material = std::dynamic_pointer_cast<ITAGeo::Material::CScalarMaterial>( material ) )
			{
				auto ppl_material = std::make_shared<propagation_path_list::ScalarMaterial>( );

				ppl_material->absorption = scalar_material->GetAbsorptionCoefficient( );
				ppl_material->scattering = std::abs( scalar_material->cdScatteringCoefficient );

				ppl.material_database[material_id] = ppl_material;
			}
		}
	}

	return ppl;
}