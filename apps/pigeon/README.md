# PIGEON

**P**igeon **I**nvestigates **GE**ometric-acoustic **O**utdoor **N**oise

---

Pigeon is the propagation path finder application based on the image edge model.
The image edge model is a geometrical acoustics model for determining sound paths in complex urban environments considering reflections and diffractions.
Visit our website for more information on [pigeon](https://www.virtualacoustics.de/GA/iem/#pigeon) and the underlying [image edge model](https://www.virtualacoustics.de/GA/iem/).

## Getting started

To get started with pigeon, check out the respective [section](https://www.virtualacoustics.de/GA/iem/#getting-started) on our website.

### Python interface

In the downloaded pigeon package, a python interface is included as a wheel file.
The python interface can be installed using pip.

```bash
pip install pigeon-<version>-py3-none-any.whl
```

Where <version> is the version of the pigeon package.
The python interface provides a high-level interface to the pigeon application.
It allows to define a scene, calculate the propagation paths and, if Matlab and the ITA-toolbox is installed, generate a propagation filter.

### Examples

The python interface includes several examples to get started.
The examples can be found in the `examples` folder of the package.

When the python interface is installed, the examples can be run from the command line:

```bash
python -m pigeon.examples.<example_name>
```

Or via the included example runner:

```bash
python -m pigeon.examples.__main__
```

### Coordinate system

All coordinates in pigeon are given in OpenGL coordinates.

## Development of the python interface

The python interface uses hatch as a build tool.
It provides environment handling and allows to build the package.
This also allows to run the examples using hatch:

Navigate to your directory, e.g.:
```bash
cd apps/pigeon/python/
```

Run the examples:
```bash
hatch run examples
```
This generates a new folder with at least three files: `pigeon.ini`, `propagation_filter.ita`, and `result.json`. Depending on the example, you might also find a `geometry.dae` in the same folder.

## Publication

An article about the underlying [image edge model](https://doi.org/10.1051/aacus/2021010) was published in Acta Acustica, Volume 5, 2021.

## Download

Ready-to-use binaries of the pigeon app can be downloaded [here](https://www.virtualacoustics.de/GA/iem/#pigeon).

## Quick build guide

Pigeon uses CMake as a build tool, see [ITACoreLibs](http://git.rwth-aachen.de/ita/ITACoreLibs/wikis/home) for more information.
It can be build from the ITAGeometricalAcoustics root folder.

## License

See LICENSE.md for licensing information.
