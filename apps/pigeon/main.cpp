/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <CLI/App.hpp>
#include <CLI/Config.hpp>
#include <CLI/Formatter.hpp>

int main_cli( const std::string &config, bool quite, const std::string &logFile, bool new_ppl_format );
int main_gui( int argc, char *argv[] );

int main( int argc, char *argv[] )
{
	CLI::App app {
		R"(
Pigeon investigates geometric-acoustic outdoor noise.
For this it uses the image edge model.

There are two ways of running a simulation using this application.
If no arguments are supplied, a GUI will open in which many simulation parameters can be set.
The other, and preferred, option is to supply a configuration file.
In it, all possible simulation parameters can be configured.

All source and receiver position must be given in OpenGL coordinates.
)"
	};

	std::string configFile;
	std::string logFile { "pigeon.log" };
	bool quiet          = false;
	bool new_ppl_format = false;
	app.add_option( "config", configFile, "Configuration file for CLI simulation." )->check( CLI::ExistingFile );
	app.add_flag( "--non-verbose,-V", quiet, "Enable non-verbose mode for CLI simulation." );
	app.add_option( "--log-file,-l", logFile, "File in which to save the log, -1 for no file log" )
	    ->type_name( "FILE" )
	    ->capture_default_str( )
	    ->transform(
	        []( const std::string &arg )
	        {
		        if( arg == "-1" )
		        {
			        return std::string { };
		        }
		        return arg;
	        } );
	app.add_flag( "--ppl", new_ppl_format, "Use the new PPL format, this will be the default in the future." );
	app.set_version_flag( "-v,--version", PIGEON_VERSION );

	CLI11_PARSE( app, argc, argv );

	if( argc == 1 )
	{
		std::cerr << "\x1b[7m\x1b[31m" << "Starting from version 2024a the GUI will not support the full feature\n"
		          << "set of pigeon. Please use the included python interface." << "\x1b[0m" << '\n';
	}

	return argc == 1 ? main_gui( argc, argv ) : main_cli( configFile, quiet, logFile, new_ppl_format );
}
