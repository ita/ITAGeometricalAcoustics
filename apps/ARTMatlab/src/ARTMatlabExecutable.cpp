/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 *
 *
 */

// ITA includes
// #include <ITAException.h>
#include <ITAStopWatch.h>

// ITAGeo includes
#include <ART_instrumentation.h>
#include <IHTA/Instrumentation/matlab_sink.h>
#include <ITAGeo/Atmosphere/StratifiedAtmosphere.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Utils/JSON/Atmosphere.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/EigenraySearch/EigenrayEngine.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/EigenraySearch/EigenraySettings.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Rays.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Simulation/ARTEngine.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Simulation/ARTSettings.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Utils/RayToPropagationPath.h>
#include <StratifiedAtmosphere_instrumentation.h>
#include <spdlog/sinks/ostream_sink.h>


// Matlab includes
#include <matrix.h>
#include <mex.h>

// STD includes
#include <memory>
#include <string>
#include <vector>


using namespace ITAGeo;
// using namespace ITAGeo::Utils;
using namespace ITAPropagationPathSim::AtmosphericRayTracing;
using namespace ITAPropagationPathSim::AtmosphericRayTracing::Utils;

//----PARSING ATMOSPHERE---
//-------------------------
ITAGeo::CStratifiedAtmosphere ParseAtmosphere( const mxArray* mxaAtmosphereJSON, std::string sInputPosition = "First" )
{
	if( !mxIsChar( mxaAtmosphereJSON ) )
		mexErrMsgIdAndTxt( "ART:notJSONString", ( sInputPosition + " input argument must be a JSON string representing a stratified atmosphere" ).c_str( ) );

	try
	{
		const std::string sJsonAtmosphere = mxArrayToString( mxaAtmosphereJSON );
		auto atmosphere                   = ITAGeo::CStratifiedAtmosphere( );
		ITAGeo::Utils::JSON::Decode( atmosphere, sJsonAtmosphere );
		return atmosphere;
	}
	catch( std::exception e )
	{
		// mexErrMsgIdAndTxt("ART:invalidJSONString", ("Error during conversion from JSON format to CStratifiedAtmosphere. Probably due to errorneous string. Error
		// message: " + std::string(e.what())).c_str() );
		mexErrMsgIdAndTxt( "ART:invalidJSONString", "Error during conversion from JSON format to CStratifiedAtmosphere. Probably due to errorneous string." );
	}
}

//----PARSING GENERAL DATA---
//---------------------------
#pragma region PARSING GENERAL DATA
VistaVector3D ParseVistaVector3D( const mxArray* mxaVector3D, const std::string sInputPosition )
{
	bool bCorrect = mxIsDouble( mxaVector3D ) && mxGetNumberOfElements( mxaVector3D ) == 3 && !mxIsComplex( mxaVector3D );
	if( !bCorrect )
		mexErrMsgIdAndTxt( "ART:not3DVector", ( sInputPosition + " input must be a real-valued 1x3 numeric vector" ).c_str( ) );

	double* data = mxGetPr( mxaVector3D );
	return VistaVector3D( data[0], data[1], data[2] );
}
std::vector<VistaVector3D> ParseMultipleVistaVector3D( const mxArray* mxaVectors3D, const std::string sInputPosition )
{
	int nDim            = mxGetNumberOfDimensions( mxaVectors3D );
	const mwSize* iSize = mxGetDimensions( mxaVectors3D );
	bool bCorrect       = mxIsDouble( mxaVectors3D ) && nDim == 2 && iSize[1] == 3 && iSize[0] > 0;
	if( !bCorrect )
		mexErrMsgIdAndTxt( "ART:not3DVectors", ( sInputPosition + " input must be a real-valued Nx3 numeric vector" ).c_str( ) );

	const int nElements = iSize[1];
	const int nVectors  = iSize[0];
	double* data        = mxGetPr( mxaVectors3D );
	std::vector<VistaVector3D> v3Vectors( nVectors );
	for( int idx = 0; idx < nVectors; idx++ )
	{
		v3Vectors[idx][Vista::X] = data[idx];
		v3Vectors[idx][Vista::Y] = data[idx + nVectors];
		v3Vectors[idx][Vista::Z] = data[idx + 2 * nVectors];
	}
	return v3Vectors;
}

mxArray* GetField( const mxArray* mxaStruct, const std::string& sFieldname )
{
	int iField = mxGetFieldNumber( mxaStruct, sFieldname.c_str( ) );
	if( iField < 0 )
		mexErrMsgIdAndTxt( "ART:invalidFieldname", ( "Fieldname: '" + sFieldname + "' not found in given struct" ).c_str( ) );

	return mxGetFieldByNumber( mxaStruct, 0, iField );
}
double GetDoubleScalarFromField( const mxArray* mxaStruct, const std::string& sFieldname )
{
	mxArray* mxData = GetField( mxaStruct, sFieldname );
	if( !mxIsDouble( mxData ) || mxGetNumberOfElements( mxData ) != 1 )
		mexErrMsgIdAndTxt( "ART:notDoubleScalar", ( "Expected field '" + sFieldname + "' to contain a double scalar" ).c_str( ) );

	return mxGetPr( mxData )[0];
}
bool GetBooleanFromField( const mxArray* mxaStruct, const std::string& sFieldname )
{
	mxArray* mxData = GetField( mxaStruct, sFieldname );
	if( !mxIsLogicalScalar( mxData ) )
		mexErrMsgIdAndTxt( "ART:notBoolean", ( "Expected field '" + sFieldname + "' to contain a logical scalar" ).c_str( ) );

	return mxGetLogicals( mxData )[0];
}
#pragma endregion

//----PARSING SETTINGS---
//-----------------------
#pragma region PARSING SETTINGS
Simulation::SolverMethod ParseSolverMethodField( const mxArray* mxaRayTracingSettings, const std::string& sFieldname )
{
	mxArray* mxSolverMethod = GetField( mxaRayTracingSettings, sFieldname );
	if( !mxIsChar( mxSolverMethod ) )
		mexErrMsgIdAndTxt( "ART:notString", ( "Expected field '" + sFieldname + "' to contain a char array" ).c_str( ) );

	const std::string sSolverMethod        = mxArrayToString( mxSolverMethod );
	Simulation::SolverMethod oSolverMethod = Simulation::RUNGE_KUTTA;
	if( sSolverMethod.compare( "euler" ) == 0 )
		oSolverMethod = Simulation::EULER;
	else if( sSolverMethod.compare( "runge-kutta" ) != 0 )
		mexErrMsgIdAndTxt( "ART:invalidSolverMethodString", "Invalid solver method string. Allowed strings are 'runge-kutta' and 'euler'." );

	return oSolverMethod;
}

Simulation::Settings ParseRayTracingSettings( const mxArray* mxaRayTracingSettings, const std::string sInputPosition )
{
	if( !mxIsStruct( mxaRayTracingSettings ) )
		mexErrMsgIdAndTxt( "ART:notStruct", ( sInputPosition + " input must be struct with settings for ray tracing" ).c_str( ) );

	Simulation::Settings rayTracingSettings;

	rayTracingSettings.solverMethod         = ParseSolverMethodField( mxaRayTracingSettings, "solverMethod" );
	rayTracingSettings.dIntegrationTimeStep = GetDoubleScalarFromField( mxaRayTracingSettings, "integrationTimeStep" );
	rayTracingSettings.bMultiThreading      = GetBooleanFromField( mxaRayTracingSettings, "multiThreadingActive" );

	rayTracingSettings.adaptiveIntegration.bActive             = GetBooleanFromField( mxaRayTracingSettings, "adaptiveIntegrationActive" );
	rayTracingSettings.adaptiveIntegration.dMaxError           = GetDoubleScalarFromField( mxaRayTracingSettings, "adaptiveIntegrationMaxError" );
	rayTracingSettings.adaptiveIntegration.dUncriticalError    = GetDoubleScalarFromField( mxaRayTracingSettings, "adaptiveIntegrationUncritivalError" );
	rayTracingSettings.adaptiveIntegration.iMaxAdaptationLevel = (int)GetDoubleScalarFromField( mxaRayTracingSettings, "adaptiveIntegrationMaxLevel" );

	return rayTracingSettings;
}
std::shared_ptr<Simulation::CAbortAtMaxTime> ParseExternalMaxTimeWatcher( const mxArray* mxaRayTracingSettings, const std::string sInputPosition )
{
	if( !mxIsStruct( mxaRayTracingSettings ) )
		mexErrMsgIdAndTxt( "ART:notStruct", ( sInputPosition + " input must be struct with settings for ray tracing" ).c_str( ) );

	return std::make_shared<Simulation::CAbortAtMaxTime>( GetDoubleScalarFromField( mxaRayTracingSettings, "maxPropagationTime" ) );
}
EigenraySearch::Settings ParseEigenraySearchSettings( const mxArray* mxaEigenraySettings, const std::string sInputPosition )
{
	if( !mxIsStruct( mxaEigenraySettings ) )
		mexErrMsgIdAndTxt( "ART:notStruct", ( sInputPosition + " input must be struct with settings for eigenray search" ).c_str( ) );

	EigenraySearch::Settings eigenraySettings;

	eigenraySettings.rayTracing.maxTime                      = GetDoubleScalarFromField( mxaEigenraySettings, "maxPropagationTime" );
	eigenraySettings.rayTracing.maxReflectionOrder           = (int)GetDoubleScalarFromField( mxaEigenraySettings, "maxReflectionOrder" );
	eigenraySettings.rayTracing.bAbortOnReceiverDistIncrease = GetBooleanFromField( mxaEigenraySettings, "abortOnReceiverDistIncrease" );

	eigenraySettings.rayAdaptation.accuracy.maxReceiverRadius        = GetDoubleScalarFromField( mxaEigenraySettings, "maxReceiverRadius" );
	eigenraySettings.rayAdaptation.accuracy.maxSourceReceiverAngle   = GetDoubleScalarFromField( mxaEigenraySettings, "maxSourceReceiverAngle" );
	eigenraySettings.rayAdaptation.accuracy.maxAngleForGeomSpreading = GetDoubleScalarFromField( mxaEigenraySettings, "maxAngleForGeomSpreading" );

	eigenraySettings.rayAdaptation.advancedRayZooming.bActive   = GetBooleanFromField( mxaEigenraySettings, "advancedRayZoomingActive" );
	eigenraySettings.rayAdaptation.advancedRayZooming.threshold = GetDoubleScalarFromField( mxaEigenraySettings, "advancedRayZoomingThreshold" );

	eigenraySettings.rayAdaptation.abort.maxNAdaptations       = (int)GetDoubleScalarFromField( mxaEigenraySettings, "abortMaxNAdaptations" );
	eigenraySettings.rayAdaptation.abort.minAngleResolutionDeg = GetDoubleScalarFromField( mxaEigenraySettings, "abortMinAngleResolution" );

	return eigenraySettings;
}
#pragma endregion


//------PUBLIC FUNCTIONS------
//----------------------------
void Version( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] )
{
	if( nlhs > 1 )
		mexErrMsgIdAndTxt( "ART:FindEigenrays:nOutput", "Does not support more than one output argument" );

	const char sVersion[] = ARTMATLAB_VERSION;
	plhs[0]               = mxCreateString( sVersion );
}
#pragma region SIMULATION FUNCTIONS
void FindEigenrays( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] )
{
	if( nrhs < 3 || nrhs > 5 )
		mexErrMsgIdAndTxt( "ART:FindEigenrays:nInput", "Invalid number of input arguments. Expecting between 3 and 5 arguments." );
	if( nlhs > 2 )
		mexErrMsgIdAndTxt( "ART:FindEigenrays:nOutput", "Does not support more than two output arguments" );

	CStratifiedAtmosphere atmosphere = ParseAtmosphere( prhs[0] );
	VistaVector3D sourcePosition     = ParseVistaVector3D( prhs[1], "Second" );
	VistaVector3D receiverPosition   = ParseVistaVector3D( prhs[2], "Third" );

	EigenraySearch::CEngine engine;
	if( nrhs >= 4 )
		engine.eigenraySettings = ParseEigenraySearchSettings( prhs[3], "Fourth" );
	if( nrhs >= 5 )
		engine.simulationSettings = ParseRayTracingSettings( prhs[4], "Firth" );

	std::vector<int> viTotalNumRaysTraced;
	const std::vector<std::shared_ptr<CRay>> eigenrays = engine.Run( atmosphere, sourcePosition, receiverPosition, viTotalNumRaysTraced );
	const CPropagationPathList raysAsPropPathList      = ToPropagationPath( eigenrays );
	const std::string jsonRays                         = ITAGeo::Utils::JSON::Encode( raysAsPropPathList );
	plhs[0]                                            = mxCreateString( jsonRays.c_str( ) );
	if( nlhs > 1 )
	{
		plhs[1]          = mxCreateDoubleMatrix( viTotalNumRaysTraced.size( ), 1, mxComplexity::mxREAL );
		double* pOutData = mxGetPr( plhs[1] );
		for( int idx = 0; idx < viTotalNumRaysTraced.size( ); idx++ )
			pOutData[idx] = (double)viTotalNumRaysTraced[idx];
	}
}

void TraceRays( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] )
{
	if( nrhs < 3 || nrhs > 4 )
		mexErrMsgIdAndTxt( "ART:TraceRays:nInput", "Invalid number of input arguments. Expecting either 3 or 4 arguments." );
	if( nlhs > 1 )
		mexErrMsgIdAndTxt( "ART:TraceRays:nOutput", "Does not support more than one output argument" );

	CStratifiedAtmosphere atmosphere         = ParseAtmosphere( prhs[0] );
	VistaVector3D sourcePosition             = ParseVistaVector3D( prhs[1], "Second" );
	std::vector<VistaVector3D> rayDirections = ParseMultipleVistaVector3D( prhs[2], "Third" );

	Simulation::CEngine engine;
	if( nrhs >= 4 )
	{
		engine.settings         = ParseRayTracingSettings( prhs[3], "Fourth" );
		engine.pExternalWatcher = ParseExternalMaxTimeWatcher( prhs[3], "Fourth" );
	}

	const std::vector<std::shared_ptr<CRay>> rays = engine.Run( atmosphere, sourcePosition, rayDirections );
	const CPropagationPathList raysAsPropPathList = ToPropagationPath( rays );
	const std::string jsonRays                    = ITAGeo::Utils::JSON::Encode( raysAsPropPathList );
	plhs[0]                                       = mxCreateString( jsonRays.c_str( ) );
}
#pragma endregion

#pragma region BENCHMARK FUNCTIONS
void BenchmarkEigenraySearch( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] )
{
	if( nrhs != 6 )
		mexErrMsgIdAndTxt( "ART:BenchmarkEigenraySearch:nInput", "Invalid number of input arguments. Expecting 6 arguments." );
	if( nlhs > 1 )
		mexErrMsgIdAndTxt( "ART:BenchmarkEigenraySearch:nOutput", "Does not support more than one output argument" );

	CStratifiedAtmosphere atmosphere = ParseAtmosphere( prhs[0] );
	VistaVector3D sourcePosition     = ParseVistaVector3D( prhs[1], "Second" );
	VistaVector3D receiverPosition   = ParseVistaVector3D( prhs[2], "Third" );
	if( !mxIsDouble( prhs[3] ) || mxGetNumberOfElements( prhs[3] ) != 1 )
		mexErrMsgIdAndTxt( "ART:notDoubleScalar", "Fourth input must be a double scalar" );
	const int iNRuns = (int)( *mxGetPr( prhs[3] ) );

	EigenraySearch::CEngine engine;
	engine.eigenraySettings   = ParseEigenraySearchSettings( prhs[4], "Fifth" );
	engine.simulationSettings = ParseRayTracingSettings( prhs[5], "Sixth" );


	const size_t iArraySize[] = { iNRuns };
	plhs[0]                   = mxCreateNumericArray( 1, iArraySize, mxClassID::mxDOUBLE_CLASS, mxComplexity::mxREAL );
	double* pOutData          = mxGetPr( plhs[0] );

	auto sw = ITAStopWatch( );
	for( int idx = 0; idx < iNRuns; idx++ )
	{
		sw.start( );
		engine.Run( atmosphere, sourcePosition, receiverPosition );
		sw.stop( );
		pOutData[idx] = sw.maximum( );
		sw.reset( );
	}
}
#pragma endregion


//---MATLAB INTERFACE FUNCTION---
//-------------------------------
void mexFunction( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] )
{
	auto sink = std::make_shared<IHTA::Instrumentation::matlab_sink_mt>( );
	sink->set_pattern( IHTA::Instrumentation::LogFormat::level_logger_message );
	IHTA::Instrumentation::LoggerRegistry::add_sink( sink );

	if( nrhs < 1 )
		mexErrMsgIdAndTxt( "ART:nInput", "Expecting at least one input argument" );

	if( !mxIsChar( prhs[0] ) )
		mexErrMsgIdAndTxt( "ART:notString", "First input argument must be a command string" );

	std::string sCommand = mxArrayToString( prhs[0] );

	// try
	//{
	if( sCommand.compare( "Version" ) == 0 )
		Version( nlhs, plhs, nrhs - 1, &prhs[1] );
	else if( sCommand.compare( "FindEigenrays" ) == 0 )
		FindEigenrays( nlhs, plhs, nrhs - 1, &prhs[1] );
	else if( sCommand.compare( "TraceRays" ) == 0 )
		TraceRays( nlhs, plhs, nrhs - 1, &prhs[1] );
	else if( sCommand.compare( "BenchmarkEigenraySearch" ) == 0 )
		BenchmarkEigenraySearch( nlhs, plhs, nrhs - 1, &prhs[1] );
	else
		mexErrMsgIdAndTxt( "ART:invalidCommand", "Invalid command string" );
	//}
	// catch (ITAException & e)
	//{
	//    mexErrMsgTxt(e.ToString().c_str());
	//}
	// catch (...)
	//{
	//    mexErrMsgTxt("An internal error occured - Please contact the developer");
	//}
}
