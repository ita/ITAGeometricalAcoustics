
# ARTMatlab
ARTMatlab is a mex-extension to interface the Atmospheric Ray Tracer (ART) via Matlab.
The ART framework allows to simulate curved sound propagation through an inhomogeneous, moving atmosphere. The framework can either be used to trace rays into multiple directions looking from the source or to find eigenrays connecting a source with a receiver.

Visit our website for more information on the [Atmospheric Ray Tracing](http://virtualacoustics.org/ITAGeometricalAcoustics/art.html) framework and [ARTMatlab](http://virtualacoustics.org/ITAGeometricalAcoustics/art.html#ARTMatlab).

## License

See LICENSE.md for licensing information.


## Download
ARTMatlab can be downloaded [here](http://virtualacoustics.org/ITAGeometricalAcoustics/art.html#download).


## Getting started

To get started with ARTMatlab, check out the respective [section](http://virtualacoustics.org/ITAGeometricalAcoustics/art.html#ARTMatlab) on our website.


## Publication

An open-access [paper](https://doi.org/10.1051/aacus/2021018) about the framework and the underlying methods was published in Acta Acustica, Volume 5, 2021.


## Quick build guide

ARTMatlab uses CMake and the VistaCMakeCommon scripts, see [ITACoreLibs](http://git.rwth-aachen.de/ita/ITACoreLibs/wikis/home) for more information. It requires parts of [ITAGeometricalAcoustics](http://git.rwth-aachen.de/ita/ITAGeometricalAcoustics) and [ITACoreLibs](http://git.rwth-aachen.de/ita/ITACoreLibs).
