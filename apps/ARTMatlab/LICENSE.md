## License

The ARTMatlab source code and binary package are licensed under [Apache License Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).

The binary package makes use of the [ViSTA core libs](https://www.vr.rwth-aachen.de/software/ViSTA/). It depends on [libsndfile](http://www.mega-nerd.com/libsndfile/) and [SPLINE](https://people.sc.fsu.edu/~jburkardt/c_src/spline/spline.html), which are shared under LGPL terms. Additionally, it has a dependency to [libsamplerate](http://www.mega-nerd.com/SRC/), and the [SketchUp API](https://extensions.sketchup.com/developers/sketchup_c_api/sketchup/index.html).

*Note: Some of the dependencies above result from ARTMatlab sharing parts of its code with the [pigeon](https://git.rwth-aachen.de/ita/ITAGeometricalAcoustics/-/tree/master/apps/pigeon) app or are planned to be used in future versions. For example, ARTMatlab does not make use of the Sketchup API.*