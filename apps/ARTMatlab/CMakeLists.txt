cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

project (
	ARTMatlab
	VERSION 2023.0
	LANGUAGES CXX C
)

find_package (Matlab REQUIRED)

math (EXPR RELEASE_LETTER "${PROJECT_VERSION_MINOR}+97")
string (ASCII ${RELEASE_LETTER} RELEASE_LETTER)
set (ART_MATLAB_OUTPUT_FOLDER "ARTMatlab_v${PROJECT_VERSION_MAJOR}${RELEASE_LETTER}")

matlab_add_mex (
	NAME ${PROJECT_NAME}
	SHARED
	SRC src/ARTMatlabExecutable.cpp ARTMatlab.def
	OUTPUT_NAME ${PROJECT_NAME}
	LINK_TO ITAGeo::ITAGeo ITAPropagationPathSim::ITAPropagationPathSim ITABase::ITABase nlohmann_json::nlohmann_json
)

set_target_properties (${PROJECT_NAME} PROPERTIES FOLDER "Apps/${PROJECT_NAME}" DEBUG_POSTFIX "")

# Organize sources in folders
GroupSourcesByFolder (${PROJECT_NAME})

# Definitions
target_compile_definitions (${PROJECT_NAME} PRIVATE ARTMATLAB_VERSION=\"v${PROJECT_VERSION_MAJOR}${RELEASE_LETTER}\")

# ---Install---
# mex file, Readme and license
install (TARGETS ${PROJECT_NAME} RUNTIME DESTINATION ${ART_MATLAB_OUTPUT_FOLDER}/@AtmosphericRayTracer/private
										 COMPONENT ${PROJECT_NAME}
)
install (
	FILES "README.md" "LICENSE.md"
	DESTINATION ${ART_MATLAB_OUTPUT_FOLDER}
	COMPONENT ${PROJECT_NAME}
)
install (
	DIRECTORY "matlab/"
	DESTINATION ${ART_MATLAB_OUTPUT_FOLDER}
	COMPONENT ${PROJECT_NAME}
)

if (WIN32)
	# https://stackoverflow.com/questions/62884439/how-to-use-cmake-file-get-runtime-dependencies-in-an-install-statement

	# Transfer variables into the install script
	install (CODE "set(Matlab_ROOT_DIR \"${Matlab_ROOT_DIR}\")" COMPONENT ${PROJECT_NAME})
	install (CODE "set(ART_MATLAB_OUTPUT_FOLDER \"${ART_MATLAB_OUTPUT_FOLDER}\")" COMPONENT ${PROJECT_NAME})

	install (
		CODE [[
		file (
			GET_RUNTIME_DEPENDENCIES
			LIBRARIES
			$<TARGET_FILE:ARTMatlab>
			RESOLVED_DEPENDENCIES_VAR
			_r_deps
			UNRESOLVED_DEPENDENCIES_VAR
			_u_deps
			POST_EXCLUDE_REGEXES
			Windows WINDOWS Matlab MATLAB
			DIRECTORIES
			${Matlab_ROOT_DIR}/bin/win64
		)
		foreach (_file ${_r_deps})
			file (
				INSTALL
				DESTINATION ${CMAKE_INSTALL_PREFIX}/${ART_MATLAB_OUTPUT_FOLDER}/@AtmosphericRayTracer/private
				FOLLOW_SYMLINK_CHAIN FILES "${_file}"
			)
		endforeach ()
		list (LENGTH _u_deps _u_length)
		if ("${_u_length}" GREATER 0)
			message (WARNING \"Unresolved dependencies detected!\n${_u_deps}\")
		endif ()
	]]
		COMPONENT ${PROJECT_NAME}
	)
endif ()
