## ITAGeometricalAcoustics

ITAGeometricalAcoustics is a collection of C++ libraries to compute sound propagation based on the geometrical acoustics (GA) approach.


### License

Copyright 2015-2024 Institute of Technical Acoustics (ITA), RWTH Aachen University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use files of this project except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


### Quick build guide

Follow instructions from Wiki pages of [ITACoreLibs](https://git.rwth-aachen.de/ita/ITACoreLibs/wikis/home) project.


### Applications

Additionally, this project includes applications which allow to interface the respective simulation tools for sound propagation:\
For sound propagation including reflection and diffraction at geometries such as buildings in urban areas, check out [pigeon](https://git.rwth-aachen.de/ita/ITAGeometricalAcoustics/-/tree/master/apps/pigeon).
If you are interested in sound propagation in a stratified atmosphere considering refraction, check out the interface to the [Atmospheric Ray Tracer](https://git.rwth-aachen.de/ita/ITAGeometricalAcoustics/-/tree/master/apps/ARTMatlab).
